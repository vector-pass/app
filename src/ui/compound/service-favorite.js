import { html } from '../../vendor/uhtml.js';
import WebComponent from '../../lib/web-component.js';

class ServiceFavorite extends WebComponent {
    static useShadow = false;

    static get observedAttributes() {
        return [
            'service',
            'favicon',
            'accountid',
        ];
    }

    openService = () => {
        this.fireEvent('showdetail', {
            id: this.accountid,
        });
    };

    get template() {
        return html`
            <vc-card
                class="ServiceFavorite"
                interactive
                title="${this.service}"
                @click="${this.openService}"
            >
                <vc-service-icon favicon="${this.favicon}"></vc-service-icon>
            </vc-card>
        `;
    }
}

customElements.define('vc-service-favorite', ServiceFavorite);
