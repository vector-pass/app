import { html } from '../../vendor/uhtml.js';
import WebComponent from '../../lib/web-component.js';

class QuickAction extends WebComponent {
    static useShadow = false;

    static get observedAttributes() {
        return ['title', 'icon', 'show'];
    }

    triggerDialog = () => {
        this.$.trigger.showDialog();
    };

    get template() {
        return html`
            <vc-dialog-trigger ref="trigger" show="${this.show}"></vc-dialog-trigger>
            <vc-card class="QuickAction" interactive @click="${this.triggerDialog}">
                <div class="QuickAction-iconContainer">
                    <iconify-icon
                        icon="${this.icon}"
                        height="42"
                    ></iconify-icon>
                </div>
                <div class="QuickAction-title Capitalized">
                    ${this.title}
                </div>
            </vc-card>
        `;
    }
}

customElements.define('vc-quick-action', QuickAction);