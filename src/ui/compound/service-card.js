import { html } from '../../vendor/uhtml.js';
import WebComponent from '../../lib/web-component.js';

class ServiceCard extends WebComponent {
    static useShadow = false;

    static get observedAttributes() {
        return [
            'service',
            'username',
            'favicon',
            'favorite',
            'accountid',
        ];
    }

    get isFavorite() {
        return this.hasAttribute('favorite');
    }

    openService = () => {
        this.fireEvent('showdetail', {
            id: this.accountid,
        });
    };

    get template() {
        return html`
            <vc-card
                class="ServiceCard"
                interactive
                @click="${this.openService}"
            >
                <div class="ServiceCard-iconContainer">
                    <vc-service-icon
                        favicon="${this.favicon}"
                        ?favorite="${this.isFavorite}"
                    ></vc-service-icon>
                </div>
                <div class="ServiceCard-service Capitalized">
                    ${this.service}
                </div> 
                <div class="ServiceCard-username">
                    ${this.username}
                </div>
            </vc-card>        
        `;
    }
}

customElements.define('vc-service-card', ServiceCard);
