import { html } from '../../vendor/uhtml.js';
import WebComponent from '../../lib/web-component.js';

class Header extends WebComponent {
    static useShadow = false;

    static get observedAttributes() {
        return ['username'];
    }

    get template() {
        return html`
            <header class="Header">
                <vc-logo></vc-logo>

                <vc-dialog-trigger show="settings">
                    <button class="Button Button--flat Button--outline">
                        ${this.username}
                    </button>
                </vc-dialog-trigger>
            </header>
        `;
    }
}

customElements.define('vc-header', Header);
