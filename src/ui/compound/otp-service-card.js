import { html } from '../../vendor/uhtml.js';
import WebComponent from "../../lib/web-component.js";

class OTPServiceCard extends WebComponent {
    static useShadow = false;

    static get observedAttributes() {
        return [
            'service',
            'username',
            'favicon',
            'favorite',
            'accountid',
            'secret',
        ];
    }

    get isFavorite() {
        return this.hasAttribute('favorite');
    }

    get template() {
        return html`
            <vc-card
                class="ServiceCard"
                outline
                flat
                interactive
                @click="${this.copyOTP}"
            >
                <div class="ServiceCard-iconContainer">
                    <vc-service-icon
                        favicon="${this.favicon}"
                        ?favorite="${this.hasAttribute('favorite')}"
                    ></vc-service-icon>
                </div>
                <div class="ServiceCard-service Capitalized">
                    ${this.service}
                </div>
                <div class="ServiceCard-username">
                    ${this.username}
                </div>
                <div class="ServiceCard-extra">
                    <vc-otp-code ref="otp" secret="${this.secret}"></vc-otp-code>
                </div>
            </vc-card>
        `;
    }

    copyOTP = async () => {
        await this.$.otp.copyCode();
    };
}

customElements.define('vc-otp-service-card', OTPServiceCard);
