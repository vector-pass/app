import { html } from '../../vendor/uhtml.js';
import WebComponent from '../../lib/web-component.js';

class Fab extends WebComponent {
    static useShadow = false;

    get template() {
        return html`
            <div class="FAB">
                <button class="FAB-button" @click="${() => this.triggered()}">
                    <iconify-icon icon="eva:plus-fill" height="38"></iconify-icon>
                </button>
            </div>
        `;
    }

    triggered() {
        this.fireEvent('addnew');
    }
}

customElements.define('vc-fab', Fab);
