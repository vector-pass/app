import { getTOTP } from '../../lib/otp.js';
import { copyText } from '../../lib/clipboard.js';
import { html } from '../../vendor/uhtml.js';
import WebComponent from '../../lib/web-component.js';

class OTPCode extends WebComponent {
    static useShadow = false;

    static get observedAttributes() {
        return ['secret'];
    }

    static DEFAULT_CODE = '...';

    constructor() {
        super();
        this.code = this.constructor.DEFAULT_CODE;
        this.interval = 1000;
    }

    connectedCallback() {
        super.connectedCallback();
        this.intervalReference = setInterval(
            async () => {
                await this.updateOTP();
            },
            this.interval
        );
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        clearInterval(this.intervalReference);
    }

    async updateOTP() {
        this.code = await this.generateOTP();
        this._render();
    }

    async generateOTP() {
        let otp;

        if (this.secret) {
            otp = await this._generateTOTP();
        } else {
            otp = '';
        }

        return otp;
    }

    async _generateTOTP() {
        return await getTOTP(this.secret);
    }

    async copyCode() {
        if (this.code === this.constructor.DEFAULT_CODE) return;

        await copyText(this.code);
    }

    secretCallback() {
        this.updateOTP();
    }

    get template() {
        return html`
            ${this.code}
        `;
    }
}

customElements.define('vc-otp-code', OTPCode);
