import { html } from '../../vendor/uhtml.js';
import WebComponent from '../../lib/web-component.js';

class ServiceIcon extends WebComponent {
    static useShadow = false;

    static get observedAttributes() {
        return ['favicon', 'favorite'];
    }

    connectedCallback() {
        super.connectedCallback();

        this.classList.add('ServiceIcon');
    }

    faviconChanged(value) {
        this.style.setProperty('--ServiceIcon-url', `url('${value}')`);
    }

    get template() {
        return html`
            <div class="ServiceIcon-favorite" ?hidden="${!this.hasAttribute('favorite')}">
                <iconify-icon icon="eva:star-fill" height="24"></iconify-icon>
            </div>
        `;
    }
}

customElements.define('vc-service-icon', ServiceIcon);

