import WebComponent from '../../lib/web-component.js';

class Card extends WebComponent {
    static useShadow = false;

    static get listenedEvents() {
        return ['keydown'];
    }

    connectedCallback() {
        super.connectedCallback();

        this.classList.add('Card');

        this.setAttribute('role', 'button');
        this.setAttribute('tabindex', '0');
    }

    onkeydown(e) {
        if (e.key === " " || e.key === "Enter" || e.key === "Spacebar") {
            e.preventDefault();
            this.dispatchEvent(new Event('click'));
        }
    }
}

customElements.define('vc-card', Card);
