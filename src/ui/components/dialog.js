import { html } from '../../vendor/uhtml.js';
import WebComponent from '../../lib/web-component.js';
import dialogPolyfill from '../../vendor/dialog-polyfill.js';
import { EventBus } from '../../lib/event-bus.js';

const eventBus = new EventBus('Dialog handler');

class Dialog extends WebComponent {
    static get observedAttributes() {
        return ['heading', 'name'];
    }

    connectedCallback() {
        super.connectedCallback();

        eventBus.addEventListener('show-dialog', this._handleShowDialog);
    }

    disconnectedCallback() {
        super.disconnectedCallback();

        eventBus.removeEventListener('show-dialog', this._handleShowDialog);
    }

    _handleShowDialog = (e) => {
        const { show } = e.detail;

        if (show === this.name) {
            this.showModal();
        } else if (this.$.dialog.open) {
            this.close();
        }
    }

    firstRenderCallback() {
        dialogPolyfill.registerDialog(this.$.dialog);
    }

    showModal() {
        this.$.dialog.showModal();
        // Reset scroll
        this.$.dialog.scrollTop = 0;
    }

    close() {
        this.$.dialog.close();
    }

    get template() {
        return html`
            <dialog ref="dialog" class="Dialog Card Card--flat">
                <div class="Dialog-header">
                    <div class="Dialog-title">
                        <div class="Dialog-icon">
                            <slot name="icon"></slot>
                        </div>
                        <h2 class="Dialog-titleText Capitalized NoMargin">
                            ${this.heading}
                        </h2>
                    </div>
                    <form method="dialog">
                        <button class="IconButton Button--flat">
                            <iconify-icon icon="eva:close-fill" height="24"></iconify-icon>
                        </button>
                    </form>
                </div>
                <slot></slot>
            </dialog>
        `;
    }
}

customElements.define('vc-dialog', Dialog);

class DialogTrigger extends WebComponent {
    static useShadow = false;

    static get observedAttributes() {
        return ['show']
    }

    static get listenedEvents() {
        return ['click'];
    }
    
    showDialog() {
        eventBus.fireEvent('show-dialog', {
            show: this.show,
        });
    }

    onclick() {
        this.showDialog();
    }
}

customElements.define('vc-dialog-trigger', DialogTrigger);
