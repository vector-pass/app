import { html } from '../../vendor/uhtml.js';
import WebComponent from '../../lib/web-component.js';

class IconButton extends WebComponent {
    static get observedAttributes() {
        return ['icon'];
    }

    get template() {
        return html`
            <button class="IconButton Button--outline">
                <iconify-icon icon="${this.icon}" height="24"></iconify-icon>
            </button>
        `;
    }
}

customElements.define('vc-icon-button', IconButton);
