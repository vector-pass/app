import { html } from '../../vendor/uhtml.js';
import WebComponent from '../../lib/web-component.js';

class Item extends WebComponent {
    static useShadow = false;

    static get observedAttributes() {
        return ['icon', 'label'];
    }
    
    get template() {
        return html`
            <button class="Item">
                <div class="Item-icon">
                    <iconify-icon icon="${this.icon}" height="32"></iconify-icon>
                </div>
                <div class="Item-label">${this.label}</div>
            </button>
        `;
    }
}

customElements.define('vc-item', Item);
