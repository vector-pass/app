import { html } from '../../vendor/uhtml.js';
import WebComponent from '../../lib/web-component.js';
import { createStore } from '../../lib/store.js';
import { copyText } from '../../lib/clipboard.js';

class FieldDisplay extends WebComponent {
    #store;

    static useShadow = true;

    static get observedAttributes() {
        return ['label', 'value', 'copy', 'url', 'otp', 'private'];
    }

    constructor() {
        super();

        const initialState = {
            hideValue: true,
        };

        this.#store = createStore(initialState, () => {
            this._render();
        });
    }

    get template() {
        return html`
            <div class="Field">
                <div class="Field-label">${this.label}</div>
                <div class="Field-value">${this.getValue()}</div>
                <div class="Field-actions">
                    ${this.getVisibilityAction()}
                    ${this.getCopyAction()}
                    ${this.getOpenURLAction()}
                </div>
            </div>
        `;
    }

    get isOTP() {
        return this.hasAttribute('otp');
    }

    get isPrivate() {
        return this.hasAttribute('private');
    }

    get canCopy() {
        return Boolean(this.getAttribute('copy'));
    }

    get canOpenURL() {
        return Boolean(this.getAttribute('url'));
    }

    get hideValue() {
        return this.#store.hideValue; 
    }

    set hideValue(value) {
        this.#store.hideValue = Boolean(value); 
    }

    getVisibilityAction() {
        if (!this.isPrivate) return '';

        const icon = this.hideValue
            ? 'eva:eye-outline'
            : 'eva:eye-off-outline';

        const toggleHideValue = () => this.hideValue = !this.hideValue;

        return html`
            <vc-icon-button icon="${icon}" @click="${toggleHideValue}"></vc-icon-button>
        `;
    }

    getCopyAction() {
        if (!this.canCopy) return '';

        const copyValue = async () => {
            let value = this.copy;

            if (this.isOTP) {
                value = await this.$.otp.generateOTP();
            }

            await copyText(value);
        };

        return html`
            <vc-icon-button icon="eva:copy-outline" @click="${copyValue}"></vc-icon-button>
        `;
    }

    getOpenURLAction() {
        if (!this.canOpenURL) return '';

        const openURL = () => window.open(this.url, '_blank');

        return html`
            <vc-icon-button icon="eva:external-link-outline" @click="${openURL}"></vc-icon-button>
        `;
    }

    getValue() {
        if (this.isOTP) return html`
            <vc-otp-code ref="otp" secret="${this.value}"></vc-otp-code>
        `;

        return (this.isPrivate && this.hideValue)
            ? Array((this.value || '').length).fill('*').join('')
            : this.value;
    }
}

customElements.define('vc-field-display', FieldDisplay);
