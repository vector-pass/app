import WebComponent from '../../lib/web-component.js';
import { EventBus } from '../../lib/event-bus.js';

const eventBus = new EventBus('Install Prompt');

let defferedInstallPrompt = null;
window.addEventListener('DOMContentLoaded', () => {
    window.addEventListener('beforeinstallprompt', async (e) => {
        e.preventDefault();
        defferedInstallPrompt = e;
        eventBus.fireEvent('show-prompt');
    });
});

const installApp = async () => {
    if (defferedInstallPrompt) {
        defferedInstallPrompt.prompt();

        const { outcome } = await defferedInstallPrompt.userChoice;

        // Act on the user's choice
        if (outcome === 'accepted') {
            console.log('User accepted the install prompt.');
        } else if (outcome === 'dismissed') {
            console.log('User dismissed the install prompt');
        }

        // The defferedInstallPrompt can only be used once.
        defferedInstallPrompt = null;
        eventBus.fireEvent('hide-prompt');
    }
};

class InstallButton extends WebComponent {
    static useShadow = false;

    static get listenedEvents() {
        return ['click'];
    }
    
    connectedCallback() {
        super.connectedCallback();
        
        if (defferedInstallPrompt === null) this.hidePrompt();
        
        eventBus.addEventListener('show-prompt', this.showPrompt);
        eventBus.addEventListener('hide-prompt', this.hidePrompt);
    }
    
    disconnectedCallback() {
        super.disconnectedCallback();
        
        eventBus.removeEventListener('show-prompt', this.showPrompt);
        eventBus.removeEventListener('hide-prompt', this.hidePrompt);
    }
    
    hidePrompt = () => {
        this.setAttribute('hidden', '');
    };
    
    showPrompt = () => {
        this.removeAttribute('hidden');
    };
    
    onclick() {
        installApp();
    }
}

customElements.define('vc-install-button', InstallButton);
