import { html } from '../vendor/uhtml.js';

const isItemVisible = (item) => item.visible || item.visible === undefined;

const FieldAction = ({icon, action, visible}) => html`
    <button class="IconButton Button--outline" @click="${action}" ?hidden="${!isItemVisible({visible})}">
        <iconify-icon icon="${icon}" height="24"></iconify-icon>
    </button>
`;

const FieldActions = (data = []) => {
    const actions = data
        //.map((item) => html.for(item)`${FieldAction(item)}`);
        .map((item) => FieldAction(item));
    return html`
        <div class="Field-actions">${actions}</div>
    `;
};

export const Field = ({label, value, actions}) => html`
    <div class="Field">
        <div class="Field-label">${label}</div>
        <div class="Field-value">${value}</div>
        ${FieldActions(actions)}
    </div>
`;

export const Fields = (data = []) => {
    const fields = data
        .filter(isItemVisible)
        //.map((item) => html.for(item)`${Field(item)}`);
        .map((item) => Field(item));
    return html`
        <div class="Field-container">${fields}</div>
    `;
}

export const InputLabel = (label, input) => html`
    <label class="InputLabel">
        <div class="InputLabel-label Field-label">${label}</div>
        ${input}
    </label>
`;
