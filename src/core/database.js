import PouchDB from '../vendor/pouchdb.js';

/** @typedef {import('./index.js').Account} Account */

/**
 * Extract account from docs.
 *
 * @param {Function} transform
 * @param {Account} item
 *
 * @returns {Promise<Account>}
 */
const extractAccount = async (transform, item) => {
    return await transform(item);
};

/**
 * Extract all account from docs.
 *
 * @param {Function} transform
 * @param {Array} rows
 *
 * @returns {Promise<Account[]>}
 */
const extractAccounts = async (transform, rows) => {
    return await Promise.all(rows.map((item) => transform(item.doc)));
};

export class Database {
    /**
     * @property {PouchDB} #db
     */
    #db;

    /**
     * @property {PouchDB} #remoteDB
     */
    #remoteDB;

    /**
     * @property {Function} #encrypt
     */
    #encrypt;

    /**
     * @property {Function} #decrypt
     */
    #decrypt;

    /**
     * @property {Function} #encryptAll
     */
    #encryptAll;

    /**
     * @property {Function} #decryptAll
     */
    #decryptAll;

    /**
     * @property {Object} #changes
     */
    #changes;

    /**
     * setUp database
     *
     * @param {string} databaseName
     * @param {Function} encrypt
     * @param {Function} decrypt
     */
    setUp(databaseName, encrypt, decrypt) {
        this.#db = PouchDB(databaseName);

        this.#encrypt = extractAccount.curry(encrypt);
        this.#decrypt = extractAccount.curry(decrypt);

        this.#encryptAll = extractAccounts.curry(this.#encrypt);
        this.#decryptAll = extractAccounts.curry(this.#decrypt);
    }

    async setUpSyncDatabase() {
        let remoteDatabaseUrl;

        try {
            const doc = await this.#db.get('_local/remoteDatabase');
            remoteDatabaseUrl = doc.url;
        } catch (e) {
            console.log('Remote database not found');
            return;
        }

        try {
            this.#remoteDB = PouchDB(remoteDatabaseUrl);

            this.#db.sync(this.#remoteDB, { live: true, retry: true });

        } catch (e) {
            console.error('Remote sync failed:', e);
            return;
        }
    }

    async setSyncRemoteDatabase(url) {
        const remoteDB = PouchDB(url);

        try {
            const info = await remoteDB.info();
            console.log(info);

            await this.#db.put({_id: '_local/remoteDatabase', url});
            await this.setUpSyncDatabase();

            return info.db_name;

        } catch (e) {
            console.log(e);
            throw new Error('Database failed');
        }
    }

    /**
     * Listen and register a callback for any changes
     *
     * @param {Function} changeCallback
     */
    listenForChanges(changeCallback) {
        if (!this.#db) return;

        const callback = async (item) => {
            changeCallback(await this.#decrypt(item.doc));
        };
        this.#changes = this.#db
            .changes({ since: 'now', live: true, include_docs: true })
            .on('change', callback)
            .on('error', console.error);
    }

    /**
     * Get all accounts
     *
     * @returns {Promise<Account[]>}
     */
    async getAllAccounts() {
        if (!this.#db) return [];

        try {
            const { rows } = await this.#db.allDocs({
                include_docs: true,
                descending: true,
            });
            return await this.#decryptAll(rows);
        } catch (err) {
            console.error(err);
        }
    }

    /**
     * Get an account
     *
     * @param {string} id
     *
     * @returns {Promise<Account|undefined>}
     */
    async getAccount(id) {
        if (!this.#db) return;

        try {
            const dbAccount = await this.#db.get(id);
            return await this.#decrypt(dbAccount);
        } catch (err) {
            console.error(err);
        }
    }

    /**
     * Add an account
     *
     * @param {Account} accountData
     *
     * @returns {Promise<Account|undefined>}
     */
    async addAccount(accountData) {
        if (!this.#db) return;

        try {
            const id = accountData._id || new Date().toJSON();
            await this.#db.put({
                ...(await this.#encrypt(accountData)),
                _id: id,
                created: new Date().toJSON(),
                updated: new Date().toJSON(),
            });
            return await this.getAccount(id);
        } catch (err) {
            console.error(err);
        }
    }

    /**
     * Edit an account
     *
     * @param {string} id
     * @param {Account} accountData
     *
     * @returns {Promise<Account|undefined>}
     */
    async editAccount(id, accountData) {
        if (!this.#db) return;

        try {
            const doc = await this.getAccount(id);
            await this.#db.put({
                ...(await this.#encrypt(doc)),
                ...(await this.#encrypt(accountData)),
                _id: id,
                updated: new Date().toISOString(),
            });
            return await this.getAccount(id);
        } catch (err) {
            console.error(err);
        }
    }

    /**
     * Delete an account
     *
     * @param {string} id
     */
    async deleteAccount(id) {
        if (!this.#db) return;

        try {
            const doc = await this.#db.get(id);
            await this.#db.remove(doc);
        } catch (err) {
            console.error(err);
        }
    }

    /**
     * Export entire raw database
     *
     * @returns {Promise<Account[]|undefined>}
     */
    async exportAccounts() {
        try {
            const { rows } = await this.#db.allDocs({
                include_docs: true,
                descending: true,
            });
            return rows.map(item => item.doc);
        } catch (err) {
            console.error(err);
        }
    }

    /**
     * Import entire raw database
     *
     * @param {Account[]} accounts
     */
    async importAccounts(accounts=[]) {
        try {
            for (const { _id, _rev, ...data } of accounts) {
                const doc = await this.#db
                    .get(_id)           // Get the existing account
                    .catch(() => ({})); // or an empty object if missing

                await this.#db.put({
                    ...doc,
                    ...data,
                    _id: _id || new Date().toISOString(),
                    updated: new Date().toISOString(),
                });
            }
        } catch (err) {
            console.error(err);
        }
    }

    /**
     * Clear the whole database
     */
    async wipe() {
        if (!this.#db) return;

        this.#changes.cancel();
        await this.#db.destroy();
    }

    /**
     * Close the database
     */
    async close() {
        if (!this.#db) return;

        this.#changes.cancel();
        await this.#db.close();
    }
}

