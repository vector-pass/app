import { AuthManager } from './auth.js';
import { AccountManager } from './account.js';
import { getDisplayFields } from './fields/display.js';
import { getFormFields } from './fields/form.js';
import { getNormalizeAccount } from './fields/normalizer.js';
import { exportToJsonFile, importFromFile, importFromUrl } from '../lib/import-export.js';
import { sendNotification } from '../lib/notification.js';

/** @typedef {import('./index.js').Auth} Auth */
/** @typedef {import('./index.js').Account} Account */

/**
 * Main core for all the business logic
 */
export class Core {
    #authManager;
    #accountManager;

    /**
     * @param {Function} storeCallback
     */
    constructor(storeCallback) {
        this.#authManager = new AuthManager(storeCallback);
        this.#accountManager = new AccountManager(storeCallback);
    }

    /**
     * Get auth store
     *
     * @returns {Auth}
     */
    get auth() {
        return this.#authManager.auth;
    }

    /**
     * Get accounts store all
     *
     * @returns {Account[]}
     */
    get accounts() {
        return this.#accountManager.accounts;
    }

    /**
     * Get accounts store favorites
     *
     * @returns {Account[]}
     */
    get favorites() {
        return this.#accountManager.favorites;
    }

    /**
     * Get accounts store otp
     *
     * @returns {Account[]}
     */
    get otp() {
        return this.#accountManager.otp;
    }

    /**
     * Get account
     *
     * @param {string} id
     *
     * @returns {Promise<Account>}
     */
    async getAccount(id) {
        return await this.#accountManager.getAccount(id);
    }

    /**
     * Create new account
     *
     * @param {Account} data
     */
	async createAccount(data) {
        const account = await getNormalizeAccount(data);
		await this.#accountManager.createAccount(account);
	}

    /**
     * Save existing account
     *
     * @param {string} id
     * @param {Account} data
     */
	async saveAccount(id, data) {
        const account = await getNormalizeAccount(data);
		await this.#accountManager.saveAccount(id, account);
	}

    /**
     * Delete an account
     *
     * @param {string} id
     */
	async deleteAccount(id) {
		await this.#accountManager.deleteAccount(id);
	}

    /**
     * Get account display fields
     *
     * @param {Account} account
     *
     * @returns {Promise<Array>}
     */
    async getAccountDisplayFields(account) {
        return await getDisplayFields(this.auth.token, account);
    }

    /**
     * Get account form fields
     *
     * @param {Account} account
     *
     * @returns {Promise<Array>}
     */
    async getAccountFormFields(account) {
        return getFormFields(account);
    }

    /**
     * Load logged account
     */
    async init() {
        await this.#authManager.loadAuth();
        await this.#accountManager.setUpDatabase(this.auth);
    }

    async syncDatabase(url) {
        this.#accountManager.setSyncRemoteDatabase(url);
    }

    /**
     * Login and set up database
     */
    async login(username, password) {
        await this.#authManager.login(username, password);
        await this.#accountManager.setUpDatabase(this.auth);
    }

    /**
     * Logout and disconnect active database
     */
    async logout() {
        await this.#authManager.logout();
        await this.#accountManager.closeDatabase();
    }

    /**
     * Export data into as a JSON file
     */
    async exportData() {
        const exportData = await this.#accountManager.exportDatabase();

        const isoString = new Date().toISOString();
        const filename = `export_${this.auth.username}_${isoString}.json`;

        exportToJsonFile(exportData, filename);

        sendNotification('Data exported successfully');
    }

    async importData(file=null, url=null) {
        let importedData = [];

        if (url) {
            importedData = await importFromUrl(url);
        } else if (file) {
            importedData = await importFromFile(file);
        }

        if (!importedData.length) return;

        await this.#accountManager.importDatabase(importedData);

        sendNotification('Data imported successfully');
    }

    async wipeData() {
        await this.#accountManager.wideData();
        await this.#accountManager.setUpDatabase(this.auth);

        sendNotification('All data has been deleted');
    }
}
