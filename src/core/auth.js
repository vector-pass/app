import { generateToken } from '../lib/generator.js';
// import { exportToJsonFile, importFromFile, importFromUrl } from '../lib/import-export.js';
// import { sendNotification } from '../lib/notification.js';
// import { Service } from '../lib/service.js';
import { getStorage } from '../lib/storage.js';
import { createStore } from '../lib/store.js';

/** @typedef {import('./index.js').Auth} Auth */

/**
 * Get an immutable Auth object
 *
 * @param {Auth['username']} username
 * @param {Auth['token']} token
 *
 * @returns {Auth}
 */
const getAuth = (username='', token='') => Object.freeze({
    username,
    token,
    isAuthenticated: !!username,
});

/**
 * Control all auth logic
 */
export class AuthManager {
    #store;
    #storage;

    /**
     * @param {Function} storeCallback
     */
    constructor(storeCallback) {
        const initialState = {
            auth: getAuth(),
        };

        this.#store = createStore(initialState, storeCallback);

        this.#storage = getStorage(sessionStorage, 'auth');
    }

    /**
     * Get auth store
     *
     * @returns {Auth}
     */
    get auth() {
        return this.#store.auth;
    }

	/**
	 * Load active account from temporary storage
	 */
    async loadAuth() {
        const { username, token } = await this.#storage.load() || {};
        this.#store.auth = getAuth(username, token);
    }

	/**
	 * Login and store active account in temporary storage
	 */
    async login(username, password) {
        const token = await generateToken(username, password);
        await this.#storage.save({ username, token});
        this.#store.auth = getAuth(username, token);
    }

	/**
	 * Logout active account and clear temporary storage
	 */
    async logout() {
        await this.#storage.clear();
        this.#store.auth = getAuth();
    }
}
