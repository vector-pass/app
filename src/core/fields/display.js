import { generatePassword, getAllowedCharacters } from '../../lib/generator.js';

/** @typedef {import('../index.js').Account} Account */
/** @typedef {import('../index.js').DisplayField} DisplayField */

/**
 * Get service display field from account.
 *
 * @param {string} _token
 * @param {Account} account
 *
 * @returns {Promise<DisplayField|null>}
 */
const getService = async (_token, account) => {
    let url = '';

    if ((account.service || '').includes('.')) {
        url = `https://${account.service}`;
    }
    
    return {
        label: 'Service',
        value: account.service,
        url,
        isPrivate: false,
    };
};

/**
 * Get url display field from account.
 *
 * @param {string} _token
 * @param {Account} account
 *
 * @returns {Promise<DisplayField|null>}
 */
const getURL = async (_token, account) => {
    return {
        label: 'URL',
        value: account.url,
        url: account.url,
        isPrivate: false,
    };
};

/**
 * Get username display field from account.
 *
 * @param {string} _token
 * @param {Account} account
 *
 * @returns {Promise<DisplayField|null>}
 */
const getUsername = async (_token, account) => ({
    label: 'Username',
    value: account.username,
    copy: account.username,
    isPrivate: false,
});

/**
 * Get password display field from account.
 *
 * @param {string} token
 * @param {Account} account
 *
 * @returns {Promise<DisplayField|null>}
 */
const getPassword = async (token, account) => {
    if (!account.password.length) return null;

    const allowedCharacters = getAllowedCharacters(
        account.password.lowercase,
        account.password.uppercase,
        account.password.numbers,
        account.password.special
    );

    const password = await generatePassword(
        token,
        account.service,
        account.username,
        account.password.counter,
        account.password.length,
        allowedCharacters
    );

    return {
        label: 'Password',
        value: password,
        copy: password,
        isPrivate: true,
    };
};

/**
 * Get pin display field from account.
 *
 * @param {string} token
 * @param {Account} account
 *
 * @returns {Promise<DisplayField|null>}
 */
const getPin = async (token, account) => {
    if (!account.pin.length) return null;

    const lowercase = false;
    const uppercase = false;
    const numbers = true;
    const special = false;

    const allowedCharacters = getAllowedCharacters(
        lowercase,
        uppercase,
        numbers,
        special
    );

    const pin = await generatePassword(
        token,
        account.service,
        account.username,
        account.pin.counter,
        account.pin.length,
        allowedCharacters
    );

    return {
        label: 'PIN',
        value: pin,
        copy: pin,
        isPrivate: true,
    };
};

/**
 * Get maildrop display field from account.
 *
 * @param {string} token
 * @param {Account} account
 *
 * @returns {Promise<DisplayField|null>}
 */
const getMaildrop = async (token, account) => {
    if (!account.maildrop.length) return null;

    const lowercase = true;
    const uppercase = true;
    const numbers = false;
    const special = false;

    const allowedCharacters = getAllowedCharacters(
        lowercase,
        uppercase,
        numbers,
        special
    );

    const address = await generatePassword(
        token,
        account.service,
        account.username,
        account.maildrop.counter,
        account.maildrop.length,
        allowedCharacters
    );

    const email = `${address}@maildrop.cc`;

    const url = `https://maildrop.cc/inbox/?mailbox=${address}`;

    return {
        label: 'Maildrop',
        value: email,
        copy: email,
        url,
        isPrivate: false,
    };
};

/**
 * Get custom fields display field from account.
 *
 * @param {string} _token
 * @param {Account} account
 *
 * @returns {Promise<DisplayField[]>}
 */
const getCustomFields = async (_token, account) => {
    return account.customFields.map(({ label, value }) => ({
        label,
        value,
        copy: value,
        isPrivate: true,
    }));
};

/**
 * Get otp display field from account.
 *
 * @param {string} _token
 * @param {Account} account
 *
 * @returns {Promise<DisplayField|null>}
 */
const getOTP = async (_token, account) => {
    return {
        label: 'OTP',
        value: account.otp,
        copy: account.otp,
        isOTP: true,
        isPrivate: false,
    };
};

/**
 * Get display fields.
 *
 * @param {String} token
 * @param {Account} account
 *
 * @returns {Promise<DisplayField[]>}
 */
export const getDisplayFields = async (token, account) => {
    const fields = await Promise.all([
        getService,
        getURL,
        getUsername,
        getPassword,
        getPin,
        getMaildrop,
        getCustomFields,
        getOTP,
    ].map(func => func(token, account)));

    return fields.flat().filter(i => i !== null && i.value);
};
