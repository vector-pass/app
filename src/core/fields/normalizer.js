import { fallbackFavicon, getBase64FromDomain } from '../../lib/favicon.js';

/** @typedef {import('../index.js').Account} Account */

const getCheckbox = value => value === 'on';

/**
 * Get normalized name from account.
 *
 * @param {Object} account
 *
 * @returns {Promise<Account>}
 */
const getName = async ({ name='' }) => ({ name });

/**
 * Get normalized service from account.
 *
 * @param {Object} account
 *
 * @returns {Promise<Account>}
 */
const getService = async ({ service='' }) => ({ service });

/**
 * Get normalized favorite from account.
 *
 * @param {Object} account
 *
 * @returns {Promise<Account>}
 */
const getFavorite = async ({ favorite='' }) => ({ favorite: getCheckbox(favorite) });

/**
 * Get normalized uRL from account.
 *
 * @param {Object} account
 *
 * @returns {Promise<Account>}
 */
const getURL = async ({ url='' }) => ({ url });

/**
 * Get normalized favicon from account.
 *
 * @param {Object} account
 *
 * @returns {Promise<Account>}
 */
const getFavicon = async ({ favicon, service='', url='' }) => {
    if (!favicon || favicon === fallbackFavicon) {
        let serviceIcon = await getBase64FromDomain(service);

        if (url && serviceIcon === fallbackFavicon) {
            const { hostname } = new URL(url);
            serviceIcon = await getBase64FromDomain(hostname);
        }

        return { favicon: serviceIcon };
    }
    return { favicon };
};

/**
 * Get normalized username from account.
 *
 * @param {Object} account
 *
 * @returns {Promise<Account>}
 */
const getUsername = async ({ username='' }) => ({ username });

/**
 * Get normalized password from account.
 *
 * @param {Object} account
 *
 * @returns {Promise<Account>}
 */
const getPassword = async ({ password={} }) => ({
    password: {
        counter: parseInt(password.counter),
        length: parseInt(password.length),
        lowercase: getCheckbox(password.lowercase),
        numbers: getCheckbox(password.numbers),
        special: getCheckbox(password.special),
        uppercase: getCheckbox(password.uppercase),
    },
});

/**
 * Get normalized pin from account.
 *
 * @param {Object} account
 *
 * @returns {Promise<Account>}
 */
const getPin = async ({ pin={} }) => ({
    pin: {
        counter: parseInt(pin.counter),
        length: parseInt(pin.length),
    },
});

/**
 * Get normalized maildrop from account.
 *
 * @param {Object} account
 *
 * @returns {Promise<Account>}
 */
const getMaildrop = async ({ maildrop={} }) => ({
    maildrop: {
        counter: parseInt(maildrop.counter),
        length: parseInt(maildrop.length),
    },
});

/**
 * Get normalized customFields from account.
 *
 * @param {Object} account
 *
 * @returns {Promise<Account>}
 */
const getCustomFields = async ({ customFields=[] }) => ({
    customFields: customFields
        .filter(field => field.disabled === 'false')
        .map(({ disabled, ...data }) => ({ ...data })),
});

/**
 * Get normalized otp from account.
 *
 * @param {Object} account
 *
 * @returns {Promise<Account>}
 */
const getOTP = async ({ otp='' }) => ({ otp: otp.replace(/ /g, '') });


/**
 * Normalize account fields.
 *
 * @param {Object} account
 *
 * @returns {Promise<Account>}
 */
export const getNormalizeAccount = async account => {
    const normalizedFields = await Promise.all([
        getName,
        getService,
        getFavorite,
        getURL,
        getFavicon,
        getUsername,
        getPassword,
        getPin,
        getMaildrop,
        getCustomFields,
        getOTP,
    ].map(func => func(account)));

    return normalizedFields.reduce(
        (account, newData) => ({ ...account, ...newData }), account
    );
}
