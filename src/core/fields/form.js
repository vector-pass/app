/** @typedef {import('../index.js').Account} Account */
/** @typedef {import('../index.js').FormSet} FormSet */

/**
 * Get the name field from account.
 *
 * @param {Account} account
 * @returns {FormSet}
 */
const getName = (account) => {
    return {
        label: 'Name',
        fields: [{
            type: 'text',
            name: 'name',
            label: 'Name',
            value: account.name,
            required: false,
        }],
    };
};

/**
 * Get the service field from account.
 *
 * @param {Account} account
 * @returns {FormSet}
 */
const getService = (account) => {
    return {
        label: 'Service',
        fields: [{
            type: 'text',
            name: 'service',
            label: 'Service',
            value: account.service,
            required: true,
        }],
    };
};

/**
 * Get the favorite field from account.
 *
 * @param {Account} account
 * @returns {FormSet}
 */
const getFavorite = (account) => {
    return {
        label: 'Favorite',
        fields: [{
            type: 'checkbox',
            name: 'favorite',
            label: 'Favorite',
            value: account.favorite,
            required: false,
        }],
    };
};

/**
 * Get the url field from account.
 *
 * @param {Account} account
 * @returns {FormSet}
 */
const getURL = (account) => {
    return {
        label: 'URL',
        fields: [{
            type: 'url',
            name: 'url',
            label: 'URL',
            value: account.url,
            required: false,
        }],
    };
};

/**
 * Get the username field from account.
 *
 * @param {Account} account
 * @returns {FormSet}
 */
const getUsername = (account) => {
    return {
        label: 'Username',
        fields: [{
            type: 'text',
            name: 'username',
            label: 'Username',
            value: account.username,
            required: true,
        }],
    };
};

/**
 * Get the password field from account.
 *
 * @param {Account} account
 * @returns {FormSet}
 */
const getPassword = (account) => {
    return {
        label: 'Password',
        fields: [
            {
                type: 'row',
                fields: [
                    {
                        type: 'number',
                        name: 'password.length',
                        label: 'Length',
                        value: account.password.length,
                        required: true,
                    },
                    {
                        type: 'number',
                        name: 'password.counter',
                        label: 'Counter',
                        value: account.password.counter,
                        required: true,
                    },
                ],
            },
            {
                type: 'checkbox',
                name: 'password.lowercase',
                label: 'Lowercase (a-z)',
                value: account.password.lowercase,
                required: false,
            },
            {
                type: 'checkbox',
                name: 'password.uppercase',
                label: 'Uppercase (A-Z)',
                value: account.password.uppercase,
                required: false,
            },
            {
                type: 'checkbox',
                name: 'password.numbers',
                label: 'Numbers (0-9)',
                value: account.password.numbers,
                required: false,
            },
            {
                type: 'checkbox',
                name: 'password.special',
                label: 'Special (@#]*)',
                value: account.password.special,
                required: false,
            },
        ],
    };
};

/**
 * Get the pin field from account.
 *
 * @param {Account} account
 * @returns {FormSet}
 */
const getPin = (account) => {
    return {
        label: 'Pin',
        fields: [
            {
                type: 'row',
                fields: [
                    {
                        type: 'number',
                        name: 'pin.length',
                        label: 'Length',
                        value: account.pin.length,
                        required: true,
                    },
                    {
                        type: 'number',
                        name: 'pin.counter',
                        label: 'Counter',
                        value: account.pin.counter,
                        required: true,
                    },
                ],
            },
        ],
    };
};

/**
 * Get the maildrop field from account.
 *
 * @param {Account} account
 * @returns {FormSet}
 */
const getMaildrop = (account) => {
    return {
        label: 'Maildrop',
        fields: [
            {
                type: 'row',
                fields: [
                    {
                        type: 'number',
                        name: 'maildrop.length',
                        label: 'Length',
                        value: account.maildrop.length,
                        required: true,
                    },
                    {
                        type: 'number',
                        name: 'maildrop.counter',
                        label: 'Counter',
                        value: account.maildrop.counter,
                        required: true,
                    },
                ],
            },
        ],
    };
};

/**
 * Get the custom fields from account.
 *
 * @param {Account} account
 * @returns {FormSet}
 */
const getCustomFields = (account) => {
    return {
        label: 'Custom Fields',
        fields: [
            {
                type: 'multi-row',
                name: 'customFields',
                values: account.customFields,
                fields: [
                    {
                        type: 'label-text',
                        name: 'label',
                        label: 'Label',
                        required: true,
                    },
                    {
                        type: 'label-text',
                        name: 'value',
                        label: 'Value',
                        required: true,
                    },
                ],
            },
        ],
    };
};

/**
 * Get the otp field from account.
 *
 * @param {Account} account
 * @returns {FormSet}
 */
const getOTP = (account) => {
    return {
        label: 'OTP',
        fields: [{
            type: 'text',
            name: 'otp',
            label: 'OTP',
            value: account.otp,
            required: false,
        }],
    };
};

/**
 * Get form fields.
 *
 * @param {Account} account
 *
 * @returns {FormSet[]}
 */
export const getFormFields = (account) => {
    return [
        getName,
        getService,
        getFavorite,
        getURL,
        getUsername,
        getPassword,
        getPin,
        getMaildrop,
        getCustomFields,
        getOTP,
    ].map(func => func(account));
};
