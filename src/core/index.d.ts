export type Auth = {
    username: string;
    token: string;
    isAuthenticated: boolean;
}

export type AccountPassword = {
    length: number;
    counter: number;
    lowercase: boolean;
    uppercase: boolean;
    numbers: boolean;
    special: boolean;
}

export type AccountPin = {
    length: number;
    counter: number;
}

export type AccountMaildrop = {
    length: number;
    counter: number;
}

export type AccountCustomField = {
    label: string;
    value: string;
}

export type Account = {
    _id?: string;
    _rev?: string;
    service: string;
    favorite: boolean,
    url: string;
    favicon: string;
    username: string;
    password: AccountPassword,
    pin: AccountPin,
    maildrop: AccountMaildrop,
    customFields: AccountCustomField[],
    otp: string;
}

export type DisplayField = {
    label: string;
    value?: string;
    copy?: string;
    url?: string;
    isOTP?: boolean;
    isPrivate: boolean;
}

export enum FormType {
    text = 'text',
    checkbox = 'checkbox',
    url = 'url',
    number = 'number',
    row = 'row',
    multiRow = 'multi-row',
};

export type FormField = {
    type: FormType;
    name: string;
    label: string;
    value: string;
    required: boolean;
}

export type FormRow = {
    type: FormType.row;
    fields: FormField[];
}

export type FormMultiRowValue = {
    label: string;
    value: string;
}

export type FormMultiRow = {
    type: FormType.multiRow;
    name: string;
    values: FormMultiRowValue[];
    fields: FormField[];
}

export type FormSet = {
    label: string;
    fields: FormRow[] | FormMultiRow[] | FormField[];
}
