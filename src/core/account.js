import { fallbackFavicon } from '../lib/favicon.js';
import { getStorage } from '../lib/storage.js';
import { createStore } from '../lib/store.js';
import { xorDecrypt, xorEncrypt } from '../lib/xor.js';
import { Database } from './database.js';

/** @typedef {import('./index.js').Account} Account */
/** @typedef {import('./index.js').Auth} Auth */

const PASSWORD_DEFAULT_OPTIONS = {
    length: 20,
    counter: 1,
    lowercase: true,
    uppercase: true,
    numbers: true,
    special: true,
};

const PIN_DEFAULT_OPTIONS = {
    length: 4,
    counter: 1,
};

const MAILDROP_DEFAULT_OPTIONS = {
    length: 10,
    counter: 1,
};

/**
 * Get an immutable Auth object
 *
 * @param {Account} account
 *
 * @returns {Account}
 */
export const getAccount = ({ password, pin, maildrop, customFields, ...data}) => {
	const _password = Object.freeze(password || PASSWORD_DEFAULT_OPTIONS);
	const _pin = Object.freeze(pin || PIN_DEFAULT_OPTIONS);
	const _maildrop = Object.freeze(maildrop || MAILDROP_DEFAULT_OPTIONS);
	const _customFields = Object.freeze((customFields || []).map(Object.freeze));
	return Object.freeze({
		...data,
		favicon: data.favicon || fallbackFavicon,
		password: _password,
		pin: _pin,
		maildrop: _maildrop,
		customFields: _customFields,
	});
};

/**
 * Run xor operation on account.
 *
 * @param {Function} xorOperator - The XOR function to use
 * @param {Account} account - The account to encrypt
 *
 * @returns {Promise<Account>}
 */
const transformAccount = async (xorOperator, token, account) => {
	const xorFunc = xorOperator(token, account.service, account.username);
	return getAccount({
		...account,
		otp: account.otp ? await xorFunc(account.otp) : '',
		customFields: await Promise.all((account.customFields || []).map(
			async ({label, value}) => ({
				label,
				value: await xorFunc(value),
			})
		)),
	});
};

/**
 * Encrypt account for storage in database.
 */
const encryptAccount = transformAccount.curry(xorEncrypt.curry());

/**
 * Decrypt account for in-memory use.
 */
const decryptAccount = transformAccount.curry(xorDecrypt.curry());

/**
 * Control all accounts logic
 */
export class AccountManager {
	#store;
	#database;

	/**
	 * @param {Function} storeCallback
	 */
	constructor(storeCallback) {
		const initialState = {
			accounts: [],
			favorites: [],
			otp: [],
		};

		this.#store = createStore(initialState, storeCallback);

		this.#database = new Database();
	}

    /**
     * Get auth store accounts
     *
     * @returns {Account[]}
     */
	get accounts() {
		return this.#store.accounts;
	}

    /**
     * Get auth store favorites
     *
     * @returns {Account[]}
     */
	get favorites() {
		return this.#store.favorites;
	}

    /**
     * Get auth store otp
     *
     * @returns {Account[]}
     */
	get otp() {
		return this.#store.otp;
	}

    /**
     * Add auth to the store without duplications and by updating data
     *
     * @param {Account[]} accounts
     */
	#addAccounts(...accounts) {
		const newIds = accounts.map(account => account._id);
		const oldAccounts = this.#store.accounts.filter(
			({ _id }) => !(newIds.includes(_id))
		);

		this.#store.accounts = [ ...accounts, ...oldAccounts ]
			.filter(({ _deleted=false }) => !_deleted)
			.sort((a, b) => {
				if (a._id < b._id) return 1;
				if (a._id > b._id) return -1;
				return 0;
			});

		this.#store.favorites = this.#store.accounts.filter(
			account => account.favorite
		);

		this.#store.otp = this.#store.accounts.filter(
			account => account.otp
		);
	}

	/**
	 * Set up DB with current user
	 *
	 * @param {Auth} auth
	 */
    async setUpDatabase(auth) {
        if (!auth.username) return;

        this.#database.setUp(
            auth.username,
            encryptAccount(auth.token),
            decryptAccount(auth.token),
        );

		const accounts = await this.#database.getAllAccounts();
		this.#addAccounts(...accounts);

        this.#database.listenForChanges(account => {
			this.#addAccounts(account);
		});

		await this.#importLegacyStorage(auth.username);

		await this.#database.setUpSyncDatabase();
    }

	async #importLegacyStorage(username) {
		const legacyStorage = getStorage(localStorage, `${username}_services`);
		const services = await legacyStorage.load();

		if (services && services.length) {
			services.reverse(); // Reverse to maintain order in new database
			await this.importDatabase(services);
			await legacyStorage.clear();
		}
	}

	async setSyncRemoteDatabase(url) {
		await this.#database.setSyncRemoteDatabase(url);
	}

	async getAccount(id) {
		return await this.#database.getAccount(id);
	}

	async createAccount(data) {
		await this.#database.addAccount(data);
	}

	async saveAccount(id, data) {
		await this.#database.editAccount(id, data);
	}

	async deleteAccount(id) {
		await this.#database.deleteAccount(id);
	}

	async exportDatabase() {
		return await this.#database.exportAccounts();
	}

	async importDatabase(accounts=[]) {
		return await this.#database.importAccounts(accounts);
	}

	async wideData() {
		await this.#database.wipe();

		this.#store.accounts = [];
		this.#store.favorites = [];
		this.#store.otp = [];

        this.#database = new Database();
	}

	async closeDatabase() {
        await this.#database.close();
        this.#database = new Database();
	}
}
