import { Identity, pipe, pipeAnd, call, condSwitch } from '../lib/frp.js';
import { fallbackFavicon, getBase64FromDomain } from '../lib/favicon.js';
/*
    {
        "service": "github.com",
        "favorite": true,
        "url": "",
        "favicon": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAEJUlEQVR4AazXU3Rk2xbG8f9cwY3TtlO5Fafd/XRt27Zt2zZfj23biEd1nLaNaOM7tcdo1Nlnt+v3uuw1jXOUSq2tomDsdTh7MVKrjCWIKUSMgyY2YtYRinuKNHFzX1/fEc6BwZml081p3/iGGe8GK+VcSKOCS2UFvx7e0DFwQR2Yu2pVWfno5E8kvmhmhVwASR7Yn/yxQz/YtGnT+Dl3YFn98lpHcA1YE3kgeKxIRW/p7X16BzGOmFR9ywojeDhfjUcM1nt4Tyx9flMLMRYfedS4YTMh/yS2FlG0NncmXO6aR9Oe27ikDNLlgn2cL3EA6QqgjePMWOCZd83ixYtLiHcg2nAJ0/6nwd7ud1WXFs4FPobYCSA4AnQJ7gfdJ9GJdBBAsE/S54qdPydb9p0mfh5fjsKyqm9zXOHJoya+aBafMrUDtLW1ecD/0+n0FYErXTiU6egFQp7Nlta11JY6f29mQ2Y/xxGog8J4xXx1cUPDPzdlMjsNoKau+f9m9hFiJK0a6u1u4yKk061LAqeRhLr/ma37My664QzeRQIz18BF8lxYT7IPNDQ0VLjoesWsjBihHUFBcCcXyXmlD0sMEWNm5RMqfK2L7naSiJ+P9PTs4iINDj5xGOO7JDB4iUNqJQ4FRXiXkSf+6KHrkUaJkdTqZCwlRrCrt7d3H3kSvQOYDRJjsNQhqyIOGyffpAliZFbtDPnEoRnk3yziJN+B7SXGsMpUavlM8iS65mU2hzhjjxPayXMZxf4ryJPS0ckXGjyPGMN2OIPHSRLaFwEjDwy+RALB4w7Z/SQxW1NT3/JVLlKqoenDhiXOpmQPuAIm7hAcIYHBb2rqWn6avTKLOW9vL6itb/66QvsPCYQOlzjvTjv+C/ob8FmI6A8mngqxb5rRSkRsAf4vwjvKi11X1jESLFiwvrS0aqw5VPgyZB8xowaSSfpL9jH6ogOQ+X8STBCRfVhobzhR+EJQDxFjIcaPzNwjo5O6DXAkKKkYvVTS44b9PNZ43LgL+DOAAxjKZAZBvyZiTMXsyuJilQeytwrtIYeM3wEhCczZLzgX4lcDA93D5I7EHz38S4lOiFh14IIfj/R29TvfrUD8UPAnSZ80b+w2SKbJY51wZpI68Ed/lfgprWlsXEjoHjNsniRfpjcOb+i5hXMXfW6808URQtvMY/3gYPdWjjNiahsbW8PQ3WLHOwF2naGHzbkxhcr+89I/gysDSJaqb/bBCpIaD0NePdLX3U0OS44Dmxeo0G4yo5U4b7RkcHBw4vQdaAnim1Si03y9Lho5MY4EUcbqssI1SN8CxsnheZ5xZi43NBP8OhuaHZ/2C1Bb27wsu65/rqlvPpS9GQ+vXLmy6Mx7oGVflC+6W2oaGlLkSzqdrlxS/4wM5QmpU9TSklNUNOIn1lyAAAMAtBLmXYmGd6oAAAAASUVORK5CYII",
        "username": "nostrangerdev",
        "password": {
            "length": 20,
            "counter": 1,
            "lowercase": true,
            "uppercase": true,
            "numbers": true,
            "special": true
        },
        "pin": {
            "length": 0,
            "counter": 1
        },
        "maildrop": {
            "length": 0,
            "counter": 1
        },
        "customFields": [
            {
                "label": "Email",
                "value": "BT5bWAM4WT8PJkwPHiRLNAUkWUQdIXhbHzVdF0EqXBM="
            }
        ],
        "otp": "Zhg+HXlqIQ54NksFcHc/cQ=="
    }
*/

/**
 * Parse and serialize data from storage.
 */
class DataParser {
	/**
     * Parse and decrypt data for in-memory manipulation.
	 *
	 * @param {Object} data - The raw data to parse.
	 */
	async parse(data) {}

	/**
     * Compute data derivatives from parsed data.
	 *
	 * @param {Object} data - The parsed data to compute.
	 */
	async compute(data) {}

	/**
     * Format and encrypt data for storage.
	 *
	 * @param {Object} data - The parsed data to serialize.
	 */
	async serialize(data) {}
}

class PlainStringParser extends DataParser {
	#key;

	constructor(key) {
		this.#key = key;
	}

	async parse(data) {
		return data[this.#key];
	}

	async compute(data) {
		return data[this.#key];
	}

	async serialize(data) {
		return data[this.#key];
	}
}

// class IDParser extends DataParser {
// 	#key;

// 	constructor(key) {
// 		this.#key = key;
// 	}

// 	async parse(data) {
// 		return data[this.#key] || new Date().toJSON();
// 	}

// 	async compute(data) {
// 		return data[this.#key];
// 	}

// 	async serialize(data) {
// 		return data[this.#key];
// 	}
// }

class FaviconParser extends DataParser {
	#faviconKey;
	#serviceKey;
	#urlKey;

	constructor(faviconKey, serviceKey, urlKey) {
		this.#faviconKey = faviconKey;
		this.#serviceKey = serviceKey;
		this.#urlKey = urlKey;
	}

	async parse(data) {
		const favicon = data[this.#faviconKey];
		const service = data[this.#serviceKey];
		const url = data[this.#urlKey];

		if (favicon && favicon !== fallbackFavicon)
			return favicon;

		let domainIcon = await getBase64FromDomain(service);
		
		if (url && domainIcon === fallbackFavicon) {
			const { hostname } = new URL(url);
			domainIcon = await getBase64FromDomain(hostname);
		}

		return domainIcon;
	}

	async compute(data) {
		return data[this.#faviconKey];
	}

	async serialize(data) {
		return data[this.#faviconKey];
	}
}

class DataManager {
	#parserSet;

	constructor(...parsersSet) {
		this.#parserSet = parsersSet;
	}

	async parse(rawData={}) {
		const data = {};

		for (const [ key, parser ] of this.#parserSet) {
			data[key] = await parser.parse(rawData);
		}

		return data;
	}

	async compute(rawData={}) {
		const data = {};

		for (const [ key, parser ] of this.#parserSet) {
			data[key] = await parser.compute(rawData);
		}

		return data;
	}

	async serialize(rawData={}) {
		const data = {};

		for (const [ key, parser ] of this.#parserSet) {
			data[key] = await parser.serialize(rawData);
		}

		return data;
	}
}

const constructObj = (value, prop) => ({ [prop]: value });

const deconstructObj = (obj, ...props) => props.reduce(
	(acc, prop) => ({ ...acc, [prop]: obj[prop]}), {}
);

const deconstructProperties = (obj, ...props) => props.map(prop => obj[prop]);

const getProperty = (obj, prop) => obj[prop];

const loadFavicon = async ({ favicon, service, url }) => {
	if (favicon && favicon !== fallbackFavicon)
		return favicon;

	let domainIcon = await getBase64FromDomain(service);

	if (url && domainIcon === fallbackFavicon) {
		const { hostname } = new URL(url);
		domainIcon = await getBase64FromDomain(hostname);
	}

	return domainIcon;
};

const isFallbackFavicon = favicon => favicon && favicon === fallbackFavicon;

const getHostname = url => new URL(url).hostname;

const faviconParser = pipe(
	call(deconstructObj, 'favicon', 'service', 'url'),
	condSwitch(
		pipe(
			call(getProperty, 'favicon'),
			isFallbackFavicon,
		),
		pipe(
			call(getProperty, 'favicon'),
			isFallbackFavicon,
		),
		pipe(
			call(getProperty, 'favicon'),
			isFallbackFavicon,
		),
	),
	loadFavicon,
);

export const loadAccount = async (account) => {
	return Identity(account)
		.map(faviconParser)
		.map(({service, url, favicon}) => {
			if (favicon && favicon !== fallbackFavicon) return favicon;

			
		})
		.emit();
};

export const dataManager = new DataManager(
	// ['_id', new IDParser('_id')],
	['_id', new PlainStringParser('_id')],
	['service', new PlainStringParser('service')],
	['favorite', new PlainStringParser('favorite')],
	['url', new PlainStringParser('url')],
	['favicon', new FaviconParser('favicon', 'service', 'url')],
	['username', new PlainStringParser('username')],
	// ['password', new PlainStringParser('password')],
	// ['pin', new PlainStringParser('pin')],
	// ['maildrop', new PlainStringParser('maildrop')],
	// ['customFields', new PlainStringParser('customFields')],
	// ['otp', new PlainStringParser('otp')],
);

