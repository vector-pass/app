import { curry } from './lib/frp.js';

// Add curry to any function
Object.defineProperty(Function.prototype, 'curry', {
    get() {
        return curry(this);
    },
});

// Polyfill structuredClone if not supported
if (!('structuredClone' in window)) {
  window.structuredClone = (value, _options) =>
    JSON.parse(JSON.stringify(value));
}

// Polyfill requestAnimationFrame with setTimeout
if (!('requestAnimationFrame' in window)) {
    const requestFallback = (callback) => window.setTimeout(callback, 1000 / 60);
    window.requestAnimationFrame = (
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        requestFallback
    );
}

if (!('cancelAnimationFrame' in window)) {
    const cancelFallback = (refId) => window.cancelTimeout(refId)
    window.cancelAnimationFrame = (
        window.webkitCancelAnimationFrame ||
        window.mozCancelAnimationFrame ||
        cancelFallback
    );
}
