export interface Function {
  curry(args: any[]): Function | any;
}
