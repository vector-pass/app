import { modules } from "./module.js";
// import { getUUID } from './uuid.js';

export class Service {
    constructor(data) {
        this.id = data._id || data.id || new Date().toJSON();

        this.profile = {};
        this._modules = [];

        for (const moduleClass of modules) {
            const moduleInstance = new moduleClass(this);

            moduleInstance.deserialize(data);

            this._modules.push(moduleInstance);
        }
    }

    getModule(key) {
        return this._modules.filter(
            module => module.key === key
        )[0];
    }

    async init(token) {
        await Promise.all(
            this._modules.map(
                (module) => module.init(token)
            )
        );
    }

    async reset(token) {
        await Promise.all(
            this._modules.map(
                (module) => module.reset(token)
            )
        );
    }

    getFields(token) {
        const fields = [];

        for (const module of this._modules) {
            fields.push(...module.getDisplay(token));
        }

        return fields;
    }

    getForm(token) {
        return this._modules.map((module) => module.getForm(token));
    }

    async submit(formData) {
        for (const module of this._modules) {
            await module.submit(formData);
        }
    }

    async transferService(token, otherToken) {
        const transferredService = new Service(this.toJSON());

        for (const module of this._modules) {
            await module.transferData(
                transferredService,
                token,
                otherToken
            );
        }

        return transferredService;
    }

    toJSON() {
        let serializedData = {
            _id: this.id,
        };

        for (const module of this._modules) {
            serializedData = {
                ...serializedData,
                ...module.serialize(),
            };
        }

        return serializedData;
    }
}