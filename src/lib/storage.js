export const getStorage = (storage, key) => ({
    save: (value) => new Promise((resolve) => {
        storage.setItem(key, JSON.stringify(value));
        resolve();
    }),

    load: () => new Promise((resolve) => {
        const value = storage.getItem(key);
        resolve(JSON.parse(value));
    }),

    clear: () => new Promise((resolve) => {
        storage.removeItem(key);
        resolve();
    }),
});

export const saveData = (key, value) => new Promise((resolve) => {
    localStorage.setItem(key, JSON.stringify(value));
    resolve();
});

export const loadData = (key) => new Promise((resolve) => {
    const value = localStorage.getItem(key);
    resolve(JSON.parse(value));
});

export const clearData = (key) => new Promise((resolve) => {
    localStorage.removeItem(key);
    resolve();
});

export const saveTempData = (key, value) => new Promise((resolve) => {
    sessionStorage.setItem(key, JSON.stringify(value));
    resolve();
});

export const loadTempData = (key) => new Promise((resolve) => {
    const value = sessionStorage.getItem(key);
    resolve(JSON.parse(value));
});

export const clearTempData = (key) => new Promise((resolve) => {
    sessionStorage.removeItem(key);
    resolve();
});
