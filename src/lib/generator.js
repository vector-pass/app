const pbkdf2 = async (password, salt) => {
    if (!window.crypto || !window.crypto.subtle) {
        throw "Your browser does not support the Web Cryptography API";
    }

    const encoder = new TextEncoder("utf-8");

    const encodedPassword = encoder.encode(password);
    const encodedSalt = encoder.encode(salt);

    const baseKey = await window.crypto.subtle.importKey(
        "raw",
        encodedPassword,
        { name: "PBKDF2" },
        false,
        [ "deriveKey" ]
    );

    const key = await window.crypto.subtle.deriveKey(
        {
            name: "PBKDF2",
            salt: encodedSalt,
            iterations: 10000,
            hash: "SHA-256"
        },
        baseKey,
        {
            name: "HMAC",
            hash: "SHA-256"
        },
        true,
        [ "sign" ]
    );

    const buffer = await window.crypto.subtle.exportKey("raw", key);

    const pbkdf2Array = Array.from(new Uint8Array(buffer));

    return pbkdf2Array;
};

const TYPE = {
    LOWER: 'a',
    UPPER: 'A',
    NUMBER: '0',
    SPECIAL: '@',
};

const CHAR_SETS = {
    [TYPE.LOWER]: Array.from('abcdefghijklmnopqrstuvwxyz'),
    [TYPE.UPPER]: Array.from('ABCDEFGHIJKLMNOPQRSTUVWXYZ'),
    [TYPE.NUMBER]: Array.from('1234567890'),
    // Adapted from: https://owasp.org/www-community/password-special-characters
    [TYPE.SPECIAL]: Array.from("!#$%&'()*+,-./:;<=>?@[]^_{|}~"),
};

const CHAR_SETS_ORDER = [
    TYPE.LOWER,
    TYPE.UPPER,
    TYPE.NUMBER,
    TYPE.SPECIAL,
];

const constructHash = (hash, length, allowedCharacters) => {
    // Get allowed characters with the right order
    const allowedChars = CHAR_SETS_ORDER.filter((char) => {
        return allowedCharacters.includes(char);
    });

    // Build template from allowed characters
    const template = Array(length).fill(null).map((_, index) => {
        const selectedChar = allowedChars[(hash[0] + index) % allowedChars.length];
        return selectedChar;
    });

    // Ensure the characters are used at least once
    allowedChars.forEach((char, index) => {
        template[(hash[0] + index) % template.length] = char;
    });

    // Compose template from character sets
    const composedTemplate = template.map((type, index) => {
        const currentCharSet = CHAR_SETS[type];
        const selectedChar = currentCharSet[hash[index + 1] % currentCharSet.length];
        return selectedChar;
    }).join('');

    return composedTemplate;
};

export const getAllowedCharacters = (lowercase = true, uppercase = true, numbers = true, special = true) => {
    const characters = [
        lowercase ? TYPE.LOWER : '',
        uppercase ? TYPE.UPPER : '',
        numbers ? TYPE.NUMBER : '',
        special ? TYPE.SPECIAL : '',
    ];
    return characters.join('');
};

export const generateToken = async (username, password) => {
    const hash = await pbkdf2(username, password);

    const allowedCharacters = getAllowedCharacters(true, true, true, true);

    const token = constructHash(hash, 40, allowedCharacters)

    return token;
};

export const generatePassword = async (masterPassword, service, serviceUsername, counter = 1, length = 20, allowedCharacters = 'aA0@') => {
    const separator = ':';

    const serviceKey = [
        service,
        serviceUsername,
        length,
        counter,
    ].filter((item) => item).join(separator);

    const hash = await pbkdf2(masterPassword, serviceKey);

    const generatedPassword = constructHash(hash, length, allowedCharacters);

    return generatedPassword;
};
