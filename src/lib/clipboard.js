export const copyText = async (text) => {
    if (!text) return;

    const result = await navigator.permissions.query({ name: "clipboard-write" });

    if (result.state == "granted" || result.state == "prompt") {
        await navigator.clipboard.writeText(text);
    }
};

