import { html } from '../vendor/uhtml.js';
import { InputLabel } from '../ui/components.js';
import { copyText } from './clipboard.js';
import { fallbackFavicon, getBase64FromDomain } from './favicon.js';
import { generatePassword, getAllowedCharacters } from './generator.js';
import { xorDecrypt, xorEncrypt } from './xor.js';

export class Module {
    #data;

    constructor(token='') {
        this.token = token;
    }

    get key() {
        return 'module';
    }

    get label() {
        return 'Module';
    }

    getSubKey(subKey) {
        return `${this.key}-${subKey}`;
    }

    get data() {
        return this.#data;
    }

    set data(data) {
        return
        this.#data = data;
    }

    async init(token) {}

    async reset(token) {}

    getDisplay(token) {
        return [
            {
                label: this.label,
                value: this.data,
            }
        ];
    }

    getForm(token) {
        const formFields = this.getFields(token);
        if (formFields === null) return html`<!-- Empty -->`;
        return html`
            <fieldset class="Form-fieldset">
                <legend class="Form-legend">${this.label}</legend>

                ${formFields}
            </fieldset>
        `;
    }

    getFields(token) {
        return html`
            <input class="Input" type="text" name="${this.key}" value="${this.data}" placeholder="${this.label}" autocapitalize="off" autocomplete="off" required />
        `;
    }

    async submit(formData) {
        const value = formData.get(this.key);
        this.data = value.trim();
    }

    serialize() {
        return {
            [this.key]: this.data,
        };
    }

    deserialize(rawData) {
        this.data = rawData[this.key];
    }

    async transferData(service, token, otherToken) {
        transferService[this.key] = this.data;
    }

    renderApp() {
        this.service.profile.renderApp();
    }
}

export class ServiceModule extends Module {
    get key() {
        return 'service';
    }

    get label() {
        return 'Service';
    }

    getDisplay() {
        return [
            {
                label: this.label,
                value: this.data,
                actions: [
                    {
                        icon: 'eva:external-link-outline',
                        action: () => this.openServiceUrl(),
                        visible: this.isServiceAnUrl(),
                    },
                ]
            },
        ];
    }

    isServiceAnUrl() {
        return this.data.includes('.');
    }

    openServiceUrl() {
        if (this.isServiceAnUrl()) {
            const url = `https://${this.data}`;
            window.open(url, '_blank');
        }
    }
}

export class FavoriteModule extends Module {
    get key() {
        return 'favorite';
    }

    get label() {
        return 'Favorite';
    }

    getDisplay() {
        return [];
    }

    getFields(token) {
        return html`
            <label>
                <span>${this.label}</span>
                <input class="Checkbox" type="checkbox" name="${this.key}" ?checked="${this.data}" />
            </label>
        `;
    }

    async submit(formData) {
        this.data = this.parseCheckbox(formData.get(this.key));
    }

    parseCheckbox(value) {
        return value === 'on';
    }
}

export class URLModule extends Module {
    get key() {
        return 'url';
    }

    get label() {
        return 'URL';
    }

    getDisplay() {
        return [
            {
                label: this.label,
                value: this.data,
                visible: this.data || '',
                actions: [
                    {
                        icon: 'eva:external-link-outline',
                        action: () => this.openUrl(),
                    },
                ]
            },
        ];
    }

    openUrl() {
        window.open(this.data, '_blank');
    }

    getFields(token) {
        return html`
            <input class="Input" type="url" name="${this.key}" value="${this.data}" placeholder="${this.label}" autocapitalize="off" autocomplete="off" />
        `;
    }
}

export class FaviconModule extends Module {
    get key() {
        return 'favicon';
    }

    get label() {
        return 'Favicon';
    }

    getDisplay() {
        return [];
    }

    getFields(token) {
        return null;
    }

    async submit() {}

    async init(token) {
        if (!this.data || this.data === fallbackFavicon) {
            let serviceIcon = await getBase64FromDomain(this.service.service);
            if (this.service.url && serviceIcon === fallbackFavicon) {
                const {hostname} = new URL(this.service.url);
                serviceIcon = await getBase64FromDomain(hostname);
            }
            this.data = serviceIcon;
        }
    }
}

export class UsernameModule extends Module {
    get key() {
        return 'username';
    }

    get label() {
        return 'Username';
    }

    getDisplay() {
        return [
            {
                label: this.label,
                value: this.data,
                visible: this.data,
                actions: [
                    {
                        icon: 'eva:copy-outline',
                        action: () => this.copyUsername(),
                    },
                ]
            }
        ];
    }

    async copyUsername() {
        await copyText(this.data);
    }
}

export class PasswordModule extends Module {
    constructor(service) {
        super(service);
        this.hiddenPassword = true;
        this.password = '';
    }

    get key() {
        return 'password';
    }

    get label() {
        return 'Password';
    }

    get data() {
        return this.service[this.key] || this.defaultOptions;
    }

    set data(data) {
        return
        this.service[this.key] = data || this.defaultOptions;
    }

    get defaultOptions() {
        return {
            length: 20,
            counter: 1,
            lowercase: true,
            uppercase: true,
            numbers: true,
            special: true,
        }
    }

    async init(token) {
        this.password = await this.generatePassword();
    }

    async reset(token) {
        this.hiddenPassword = true;
    }

    getDisplay() {
        return [
            {
                label: this.label,
                value: this.getValue(),
                visible: this.data.length,
                actions: [
                    {
                        icon: 'eva:eye-outline',
                        visible: this.hiddenPassword,
                        action: () => this.toggleHiddenPassword(),
                    },
                    {
                        icon: 'eva:eye-off-outline',
                        visible: !this.hiddenPassword,
                        action: () => this.toggleHiddenPassword(),
                    },
                    {
                        icon: 'eva:copy-outline',
                        action: () => this.copyPassword(),
                    },
                ]
            }
        ];
    }

    getValue() {
        if (this.hiddenPassword) {
            return Array(this.password.length).fill('*').join('');
        }
        return this.password;
    }

    toggleHiddenPassword() {
        this.hiddenPassword = !this.hiddenPassword;
        this.renderApp();
    }

    async copyPassword() {
        await copyText(this.password);
    }

    getFields(token) {
        return html`
            <div class="Form-row">
                ${InputLabel('Length', html`
                    <input class="Input" type="number" name="${this.getSubKey('length')}" value="${this.data.length}" placeholder="Length" step="1" required />
                `)}
                ${InputLabel('Counter', html`
                    <input class="Input" type="number" name="${this.getSubKey('counter')}" value="${this.data.counter}" placeholder="Counter" step="1" required />
                `)}
            </div>

            <label>
                <span>Lowercase (a-z)</span>
                <input class="Checkbox" type="checkbox" name="${this.getSubKey('lowercase')}" ?checked="${this.data.lowercase}" />
            </label>
            <label>
                <span>Uppercase (A-Z)</span>
                <input class="Checkbox" type="checkbox" name="${this.getSubKey('uppercase')}" ?checked="${this.data.uppercase}" />
            </label>
            <label>
                <span>Numbers (0-9)</span>
                <input class="Checkbox" type="checkbox" name="${this.getSubKey('numbers')}" ?checked="${this.data.numbers}" />
            </label>
            <label>
                <span>Special (@#]*)</span>
                <input class="Checkbox" type="checkbox" name="${this.getSubKey('special')}" ?checked="${this.data.special}" />
            </label>
        `;
    }

    async submit(formData) {
        const length = parseInt(formData.get(this.getSubKey('length')));
        const counter = parseInt(formData.get(this.getSubKey('counter')));

        const lowercase = this.parseCheckbox(formData.get(this.getSubKey('lowercase')));
        const uppercase = this.parseCheckbox(formData.get(this.getSubKey('uppercase')));
        const numbers = this.parseCheckbox(formData.get(this.getSubKey('numbers')));
        const special = this.parseCheckbox(formData.get(this.getSubKey('special')));

        this.data = {
            length,
            counter,

            lowercase,
            uppercase,
            numbers,
            special,
        };
    }

    parseCheckbox(value) {
        return value === 'on';
    }

    async generatePassword() {
        const {
            length,
            counter,

            lowercase,
            uppercase,
            numbers,
            special,
        } = this.data;

        const allowedCharacters = getAllowedCharacters(
            lowercase,
            uppercase,
            numbers,
            special
        );

        const password = await generatePassword(
            this.token,
            this.service.service,
            this.service.username,
            counter,
            length,
            allowedCharacters
        );

        return password;
    }
}

export class PINModule extends Module {
    constructor(service) {
        super(service);
        this.hiddenPin = true;
        this.pin = '';
    }

    get key() {
        return 'pin';
    }

    get label() {
        return 'PIN';
    }

    get data() {
        return this.service[this.key] || this.defaultOptions;
    }

    set data(data) {
        return
        this.service[this.key] = data || this.defaultOptions;
    }

    get defaultOptions() {
        return {
            length: 4,
            counter: 1,
        }
    }

    async init(token) {
        this.pin = await this.generatePin();
    }

    async reset(token) {
        this.hiddenPin = true;
    }

    getDisplay() {
        return [
            {
                label: this.label,
                value: this.getValue(),
                visible: this.data.length,
                actions: [
                    {
                        icon: 'eva:eye-outline',
                        visible: this.hiddenPin,
                        action: () => this.toggleHiddenPin(),
                    },
                    {
                        icon: 'eva:eye-off-outline',
                        visible: !this.hiddenPin,
                        action: () => this.toggleHiddenPin(),
                    },
                    {
                        icon: 'eva:copy-outline',
                        action: () => this.copyPin(),
                    },
                ]
            }
        ];
    }

    getValue() {
        if (this.hiddenPin) {
            return Array(this.pin.length).fill('*').join('');
        }
        return this.pin;
    }

    toggleHiddenPin() {
        this.hiddenPin = !this.hiddenPin;
        this.renderApp();
    }

    async copyPin() {
        await copyText(this.pin);
    }

    getFields(token) {
        return html`
            <div class="Form-row">
                ${InputLabel('Length', html`
                    <input class="Input" type="number" name="${this.getSubKey('length')}" value="${this.data.length}" placeholder="Length" step="1" required />
                `)}
                ${InputLabel('Counter', html`
                    <input class="Input" type="number" name="${this.getSubKey('counter')}" value="${this.data.counter}" placeholder="Counter" step="1" required />
                `)}
            </div>
        `;
    }

    async submit(formData) {
        const length = parseInt(formData.get(this.getSubKey('length')));
        const counter = parseInt(formData.get(this.getSubKey('counter')));

        this.data = {
            length,
            counter,
        };
    }

    async generatePin() {
        const {
            length,
            counter,
        } = this.data;

        const lowercase = false;
        const uppercase = false;
        const numbers = true;
        const special = false;

        const allowedCharacters = getAllowedCharacters(
            lowercase,
            uppercase,
            numbers,
            special
        );

        const pin = await generatePassword(
            this.token,
            this.service.service,
            this.service.username,
            counter,
            length,
            allowedCharacters
        );

        return pin;
    }
}

export class MaildropModule extends Module {
    constructor(service) {
        super(service);
        this.email = '';
    }

    get key() {
        return 'maildrop';
    }

    get label() {
        return 'Maildrop';
    }

    get data() {
        return this.service[this.key] || this.defaultOptions;
    }

    set data(data) {
        return
        this.service[this.key] = data || this.defaultOptions;
    }

    get defaultOptions() {
        return {
            length: 10,
            counter: 1,
        }
    }

    async init(token) {
        this.account = await this.generateAccount();
        this.email = await this.generateEmail();
    }

    getDisplay() {
        return [
            {
                label: this.label,
                value: this.email,
                visible: this.data.length,
                actions: [
                    {
                        icon: 'eva:copy-outline',
                        action: () => this.copyEmail(),
                    },
                    {
                        icon: 'eva:external-link-outline',
                        action: () => this.openInbox(),
                    },
                ]
            }
        ];
    }

    async copyEmail() {
        await copyText(this.email);
    }

    openInbox() {
        const url = `https://maildrop.cc/inbox/?mailbox=${this.account}`;
        window.open(url, '_blank');
    }

    getFields(token) {
        return html`
            <div class="Form-row">
                ${InputLabel('Length', html`
                    <input class="Input" type="number" name="${this.getSubKey('length')}" value="${this.data.length}" placeholder="Length" step="1" required />
                `)}
                ${InputLabel('Counter', html`
                    <input class="Input" type="number" name="${this.getSubKey('counter')}" value="${this.data.counter}" placeholder="Counter" step="1" required />
                `)}
            </div>
        `;
    }

    async submit(formData) {
        const length = parseInt(formData.get(this.getSubKey('length')));
        const counter = parseInt(formData.get(this.getSubKey('counter')));

        this.data = {
            length,
            counter,
        };
    }

    async generateAccount() {
        const {
            length,
            counter,
        } = this.data;

        const lowercase = true;
        const uppercase = true;
        const numbers = false;
        const special = false;

        const allowedCharacters = getAllowedCharacters(
            lowercase,
            uppercase,
            numbers,
            special
        );

        const account = await generatePassword(
            this.token,
            this.service.service,
            this.service.username,
            counter,
            length,
            allowedCharacters
        );

        return account;
    }
    
    async generateEmail() {
        const account = await this.generateAccount();
        
        const email = `${account}@maildrop.cc`;
        
        return email;
    }
}

export class CustomFieldsModule extends Module {
    constructor(service) {
        super(service);
        this.customFields = [];
    }

    get key() {
        return 'customFields';
    }

    get label() {
        return 'Custom Fields';
    }

    get data() {
        return this.service[this.key] || this.defaultOptions;
    }

    set data(data) {
        return
        this.service[this.key] = data || this.defaultOptions;
    }

    get defaultOptions() {
        return [];
    }

    async init(token) {
        this.customFields = await this.parseCustomFields();
    }

    async reset(token) {
        this.customFields = await this.parseCustomFields();
    }

    async parseCustomFields() {
        return await Promise.all(
            this.data.map(
                async ({label, value}) => {
                    const decryptedValue = await this.decryptValue(value);
                    return {
                        label,
                        value: decryptedValue,
                        deleted: false,
                        hiddenValue: true,
                        xorValue: value,
                    }
                }
            )
        );
    }

    getDisplay() {
        return this.customFields.map((field) => ({
            label: field.label,
            value: this.getValue(field),
            actions: [
                {
                    icon: 'eva:eye-outline',
                    visible: field.hiddenValue,
                    action: () => this.toggleHiddenValue(field),
                },
                {
                    icon: 'eva:eye-off-outline',
                    visible: !field.hiddenValue,
                    action: () => this.toggleHiddenValue(field),
                },
                {
                    icon: 'eva:copy-outline',
                    action: () => this.copyValue(field),
                },
            ]
        }));
    }

    getValue(field) {
        if (field.hiddenValue) {
            return Array(field.value.length).fill('*').join('');
        }
        return field.value;
    }

    toggleHiddenValue(field) {
        field.hiddenValue = !field.hiddenValue;
        this.renderApp();
    }

    async copyValue({label, value}) {
        await copyText(value);
    }

    async encryptValue(value) {
        return await xorEncrypt(
            this.token,
            this.service.service,
            this.service.username,
            value
        );
    }

    async decryptValue(value) {
        return await xorDecrypt(
            this.token,
            this.service.service,
            this.service.username,
            value
        );
    }

    async encryptFields(fields) {
        return await Promise.all(
            fields.map(
                async ({label, value}) => {
                    const encryptValue = await this.encryptValue(value);
                    return {
                        label,
                        value: encryptValue,
                    };
                }
            )
        )
    }

    getFields(token) {
        return html`
            <input type="hidden" name="${this.key}" value="${this.customFields.length}" />
            ${this.customFields.map((field, index) => html.for(field)`
                <div class="Form-row" ?hidden="${field.deleted}">
                    ${InputLabel('Label', html`
                        <input class="Input" type="text" name="${this.getSubKeyLabel(index)}" value="${field.label}" placeholder="Label" required autocomplete="off" ?disabled="${field.deleted}" />
                    `)}
                    ${InputLabel('Value', html`
                        <input class="Input" type="text" name="${this.getSubKeyValue(index)}" value="${field.value}" placeholder="Value" required autocapitalize="off" autocomplete="off" ?disabled="${field.deleted}" />
                    `)}
                    <input type="hidden" name="${this.getSubKeyDisabled(index)}" value="${field.deleted.toString()}" />
                    <button class="IconButton Button--outline" type="button" @click="${(e) => this.deleteField(e, index)}">
                        <iconify-icon icon="eva:trash-outline" height="24"></iconify-icon>
                    </button>
                </div>
            `)}
            <div class="Button-group Button-group--noMargin">
                <button class="Button Button--flat Button--outline" type="button" @click="${(e) => this.addField(e)}">Add field</button>
            </div>
        `;
    }

    addField(e) {
        e.preventDefault();
        this.customFields.push({
            label: '',
            value: '',
            deleted: false,
        });
        this.renderApp();
    }

    deleteField(e, index) {
        e.preventDefault();
        this.customFields[index].deleted = true;
        this.renderApp();
    }

    async submit(formData) {
        const customFields = [];

        const fieldsCreated = parseInt(formData.get(this.key));

        for (let i = 0; i < fieldsCreated; i++) {

            const disabled = this.isDisabled(formData.get(this.getSubKeyDisabled(i)));

            if (!disabled) {
                const label = formData.get(this.getSubKeyLabel(i)).trim();
                const value = formData.get(this.getSubKeyValue(i)).trim();

                customFields.push({
                    label,
                    value,
                });
            }
        }

        this.data = await this.encryptFields(customFields);
    }

    getSubKeyLabel(index) {
        return this.getSubKey(`${index}-label`);
    }

    getSubKeyValue(index) {
        return this.getSubKey(`${index}-value`);
    }

    getSubKeyDisabled(index) {
        return this.getSubKey(`${index}-disabled`);
    }

    isDisabled(value) {
        return value === 'true';
    }

    async transferData(service, token, otherToken) {
        const transferModule = new CustomFieldsModule(transferService);

        const customFields = await this.parseCustomFields();

        const transferEncryptedFields = await transferModule.encryptFields(customFields);

        transferService[this.key] = transferEncryptedFields;
    }
}

export class OTPModule extends Module {
    constructor(service) {
        super(service);
        this.secret = '';
        this.otpCodeEl = document.createElement('vc-otp-code');
    }

    get key() {
        return 'otp';
    }

    get label() {
        return 'OTP';
    }

    async init(token) {
        this.secret = await this.parseOTP();
        this.otpCodeEl.setAttribute('secret', this.secret);
    }

    async reset(token) {
        this.secret = await this.parseOTP();
        this.otpCodeEl.setAttribute('secret', this.secret);
    }

    async parseOTP() {
        if (!this.data) return '';
        const decryptedValue = await this.decryptValue(this.data);
        return decryptedValue;
    }

    getDisplay() {
        return [
            {
                label: this.label,
                value: this.otpCodeEl,
                visible: this.secret.length,
                actions: [
                    {
                        icon: 'eva:copy-outline',
                        action: () => this.copyValue(),
                    },
                ]
            }
        ];
    }

    async copyValue() {
        if (this.secret) {
            await copyText(await this.otpCodeEl.generateOTP());
        }
    }

    async encryptValue(value) {
        return await xorEncrypt(
            this.token,
            this.service.service,
            this.service.username,
            value
        );
    }

    async decryptValue(value) {
        return await xorDecrypt(
            this.token,
            this.service.service,
            this.service.username,
            value
        );
    }

    getFields(token) {
        return html`
            <input class="Input" type="text" name="${this.key}" value="${this.secret}" placeholder="${this.label}" autocapitalize="off" autocomplete="off" />
        `;
    }

    async submit(formData) {
        const secret = formData.get(this.key).replace(/ /g, '');

        if (secret) {
            const encryptedValue = await this.encryptValue(secret);
            this.data = encryptedValue;
        } else {
            this.data = '';
        }
    }

    async transferData(service, token, otherToken) {
        const transferModule = new OTPModule(transferService);

        const secret = await this.parseOTP();

        const transferEncryptedValue = await transferModule.encryptValue(secret);

        transferService[this.key] = transferEncryptedValue;
    }
}

export const modules = [
    ServiceModule,
    FavoriteModule,
    URLModule,
    FaviconModule,
    UsernameModule,
    PasswordModule,
    PINModule,
    MaildropModule,
    CustomFieldsModule,
    OTPModule,
];
