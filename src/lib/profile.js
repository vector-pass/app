import { generateToken } from "./generator.js";
import { exportToJsonFile, importFromFile, importFromUrl } from "./import-export.js";
import { sendNotification } from "./notification.js";
import { Service } from "./service.js";
import { clearTempData, loadData, loadTempData, saveData, saveTempData } from "./storage.js";

export class Auth {
    constructor() {
        this.username = null;
        this.token = null;
    }

    get isAuthenticated() {
        return !!this.username;
    }
    
    async retrieve() {
        const authData = await loadTempData('auth') || {};

        this.username = authData.username;
        this.token = authData.token;
    }

    async load(username, password) {
        this.username = username;
        this.token = await generateToken(username, password);
    }

    async save(username, password) {
        await this.load(username, password);

        await saveTempData('auth', {
            username,
            token: this.token,
        });
    }

    async clear() {
        this.username = null;
        this.token = null;

        await clearTempData('auth');
    }
}

export class Profile {
    constructor(app) {
        this.app = app;

        this.auth = new Auth();

        this.services = [];
    }

    renderApp() {
        this.app.renderApp();
    }

    get favorites() {
        return this.services.filter(({ favorite }) => favorite);
    }

    get otpServices() {
        return this.services.filter(({ otp }) => otp);
    }
    
    getService(serviceId) {
        for (const service of this.services) {
            if (service.id == serviceId) return service;
        }
    }

    async addService(service) {
        this.services = [
            service,
            ...this.services,
        ];

        await this.saveServices();
    }

    async editService(currentService, editedService) {
        this.services = this.services.map((item) => {
            if (item === currentService) {
                return editedService;
            }
            return item;
        });

        await this.saveServices();

        return editedService;
    }

    async deleteService(service) {
        this.services = this.services.filter((item) => item !== service);

        await this.saveServices();
    }

    async transferAccount(targetUsername, targetPassword) {
        const targetProfile = new Profile(this.app);

        await targetProfile.loadAuth(targetUsername, targetPassword);

        const services = await Promise.all(
            this.services.map(
                async (service) => {
                    const transferredService = await service.transferService(targetProfile);
                    await transferredService.init();
                    return transferredService;
                }
            )
        );

        targetProfile.services = [
            ...services,
            ...targetProfile.services,
        ];

        await targetProfile.saveServices();

        sendNotification(`Services transferred correctly to ${targetUsername}`);
    }

    async wipeData() {
        this.services = [];

        await this.saveServices();

        sendNotification('All services has been removed');
    }

    get storageKey() {
        return `${this.auth.username}_services`;
    }

    async saveServices() {
        await saveData(this.storageKey, this.services);
    }

    async loadServices() {
        const services = await loadData(this.storageKey) || [];

        await this._loadServicesData(services);
    }

    async exportData() {
        const isoString = new Date().toISOString();
        const filename = `export_${this.auth.username}_${isoString}.json`;

        exportToJsonFile(this.services, filename);

        sendNotification('Data exported successfully');
    }

    async importData(url = null, file = null) {
        let importedServices = [];

        if (url) {
            importedServices = await importFromUrl(url);
        } else if (file) {
            importedServices = await importFromFile(file);
        }

        const uniqueImportedServices = importedServices.filter(
            ({ service, username }) => !this.services.some(
                ({ service: includedService, username: includedUsername }) => {
                    const isSameService = includedService === service;
                    const isSameUsername = includedUsername === username;
                    return isSameService && isSameUsername;
                }
            )
        );

        await this._loadServicesData([
            ...uniqueImportedServices,
            ...this.services,
        ]);

        await this.saveServices();

        sendNotification('Data imported successfully');
    }

    async _loadServicesData(services) {
        this.services = await Promise.all(services.map(async (data) => {
            const loadedService = new Service(data);
            loadedService.profile = this;

            await loadedService.init();

            return loadedService;
        }));
    }
}