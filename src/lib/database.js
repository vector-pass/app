import PouchDB from '../vendor/pouchdb.js';

export class Database {
    #db;

    constructor(databaseName) {
        this.#db = PouchDB(databaseName);
    }

    listenForChanges(changeCallback) {
        this.#db.changes({
            since: 'now',
            live: true,
            include_docs: true,
        }).on('change', changeCallback);
    }

    async getFavorites() {
        // TODO: Create index
        const { rows } = await this.#db.query(function (doc, emit) {
            if (doc.favorite) {
                emit(doc.favorite);
            }
        }, { include_docs: true, descending: true });
        return rows.map(item => item.doc);
    }

    async getOTPServices() {
        // TODO: Create index
        const { rows } = await this.#db.query(function(doc, emit) {
            if (doc.otp !== '') {
                emit(doc.otp);
            }
        }, { include_docs: true, descending: true });
        return rows.map(item => item.doc);
    }

    async getAllServices() {
        const { rows } = await this.#db.allDocs({
            include_docs: true,
            descending: true,
        });
        return rows.map(item => item.doc);
    }

    async getService(serviceId) {
        return await this.#db.get(serviceId);
    }

    async addService(serviceData) {
        await this.#db.put({
            _id: serviceData._id || new Date().toJSON(),
            ...serviceData,
            created: new Date().toJSON(),
            updated: new Date().toJSON(),
        });
    }

    async editService(serviceId, serviceData) {
        const doc = await this.#db.get(serviceId);
        await this.#db.put({
            ...doc,
            ...serviceData,
            _id: serviceId,
            updated: new Date().toISOString(),
        });
        return await this.#db.get(serviceId);
    }

    async deleteService(serviceId) {
        const doc = await this.#db.get(serviceId);
        await this.#db.remove(doc);
    }

    async wipe() {
        await this.#db.destroy();
    }
}