import {generatePassword, getAllowedCharacters} from "./generator.js";

// Reference: https://codegolf.stackexchange.com/questions/207178/xor-two-strings
const xorStrings = (a, b) => {
    let s = '';

    // use the longer of the two words to calculate the length of the result
    for (let i = 0; i < Math.max(a.length, b.length); i++) {
        // append the result of the char from the code-point that results from
        // XORing the char codes (or 0 if one string is too short)
        s += String.fromCharCode(
            (a.charCodeAt(i) || 0) ^ (b.charCodeAt(i) || 0)
        );
    }

    return s;
};

const generateKey = async (masterPassword, service, serviceUsername, length) => {
    const counter = 1;
    const allowedCharacters = getAllowedCharacters(true, true, true, true);

    const key = await generatePassword(masterPassword, service, serviceUsername, counter, length, allowedCharacters);

    return key;
};

export const xorEncrypt = async (masterPassword, service, serviceUsername, value) => {
    const length = value.length;
    const key = await generateKey(masterPassword, service, serviceUsername, length);

    const encryptedValue = xorStrings(key, value);

    const b64EncryptedValue = window.btoa(encryptedValue);

    return b64EncryptedValue;
};

export const xorDecrypt = async (masterPassword, service, serviceUsername, value) => {
    if (!value) return '';

    const encryptedValue = window.atob(value);

    const length = encryptedValue.length;
    const key = await generateKey(masterPassword, service, serviceUsername, length);

    const decryptedValue = xorStrings(key, encryptedValue);

    return decryptedValue;
};
