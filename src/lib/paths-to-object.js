// Reference:
// https://stackoverflow.com/a/18937118

const assignPathToObject = (obj, path, value) => {
    const params = path.split('.');

    if (params.length === 0) return;

    const typedParams = params.map(field => {
        const isNumber = !isNaN(parseInt(field));
        return {
            isArray: isNumber,
            param: isNumber ? parseInt(field) : field,
        };
    });

    const paramsLength = typedParams.length - 1; 
    let ref = obj;

    for (let i = 0; i < paramsLength; i++) {
        const { param } = typedParams[i];
        const { isArray } = typedParams[i + 1];

        if (!ref[param]) ref[param] = isArray ? [] : {};

        ref = ref[param];
    }

    ref[typedParams[paramsLength].param] = value;
};

export const pathsToObject = (params=[]) => {
    const obj = {};
    
    for (const [path, value] of params) {
        assignPathToObject(obj, path, value.trim());
    }

    return obj;
};

