import { html, render } from '../vendor/uhtml.js';

class WebComponent extends HTMLElement {
    static useShadow = true;

    static get listenedEvents() {
        return [];
    }

    constructor() {
        super();

        this._renderRoot = this;

        if (this.constructor.useShadow) {
            this.attachShadow({ mode: 'open' });
            this._renderRoot = this.shadowRoot;
        }

        this.$ = new Proxy({}, {
            get: this.__getComponentId.bind(this)
        });

        for (const event of this.constructor.listenedEvents) {
            const callback = this[`on${event}`];
            this[`on${event}`] = callback.bind(this);
        }
    }

    __getComponentId(_target, name) {
        return this._renderRoot.querySelector(`[ref="${name}"]`);
    }

    connectedCallback() {
        super.connectedCallback &&
            super.connectedCallback();
        this._render();
        this.firstRenderCallback();

        for (const event of this.constructor.listenedEvents) {
            this.addEventListener(event, this[`on${event}`]);
        }
    }

    disconnectedCallback() {
        super.disconnectedCallback &&
            super.disconnectedCallback();

        for (const event of this.constructor.listenedEvents) {
            this.removeEventListener(event, this[`on${event}`]);
        }
    }

    attributeChangedCallback(attrName, oldValue, newValue) {
        super.attributeChangedCallback &&
            super.attributeChangedCallback(attrName, oldValue, newValue);

        if (oldValue !== newValue) {
            this[attrName] = newValue;

            // Call property update callback
            const propertyCallback = this[`${attrName}Changed`]
            if (propertyCallback) propertyCallback.call(this, newValue, oldValue);

            this._render();
        }
    }

    firstRenderCallback() {}

    get styles() {
        return null;
    }

    get template() {
        return null;
    }

    _render() {
        if (this.constructor.useShadow) {
            const inlineStyles = this.styles ? html`<style>${this.styles}</style>` : html`<!--ignore-->`;
            const template = this.template ? this.template : html`<slot></slot>`;
            render(this._renderRoot, html`
                <link rel="stylesheet" href="./style.css">
                ${inlineStyles}
                ${template}
            `);
        } else {
            if (this.template) {
                render(this._renderRoot, this.template);
            }
        }
    }

    fireEvent(eventName, detail={}, options={}) {
        this.dispatchEvent(
            new CustomEvent(
                eventName,
                {
                    detail,
                    bubbles: true,
                    composed: true,
                    ...options,
                }
            )
        );
    }
}

export default WebComponent;
