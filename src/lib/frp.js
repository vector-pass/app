export const Identity = x => ({
    map: fn => Identity(fn(x)),
    flatMap: fn => fn(x),
    emit: () => x,
});

export const curry = (fn) => fn.length === 0 ? fn() : (...args) => curry(fn.bind(null, ...args));

