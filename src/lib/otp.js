// Adapted from:
// https://gist.github.com/rafaelsq/e6a84d2f9b487917115362e39032d528

const hexToBuf = hex => {
    const bytes = [];
    for (let c = 0; c < hex.length; c += 2) {
        bytes.push(parseInt(hex.substr(c, 2), 16));
    }
    return new Uint8Array(bytes);
};

const lPad = (str, len, pad) => {
    if (len + 1 >= str.length) {
        str = Array(len + 1 - str.length).join(pad) + str;
    }
    return str;
};

const b32ToHex = base32 => {
    let base32chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';
    let bits = '';
    let hex = '';
    let i = 0;
    let chunk;
    let val;
    while (i < base32.length) {
        val = base32chars.indexOf(base32.charAt(i).toUpperCase());
        bits += lPad(val.toString(2), 5, '0');
        i++;
    }
    i = 0;
    while (i + 4 <= bits.length) {
        chunk = bits.substr(i, 4);
        hex = hex + parseInt(chunk, 2).toString(16);
        i += 4;
    }
    return hex;
};

const decToHex = s => (s < 15.5 ? '0' : '') + Math.round(s).toString(16);

const getCounter = (counter) => {
    return lPad(decToHex(Math.floor(counter / 30)), 16, '0');
};

const getEpoch = () => Math.round(new Date().getTime() / 1000.0);

export async function getHOTP(secret, counter) {
    const keyData = hexToBuf(b32ToHex(secret));
    const formatedCounter = hexToBuf(getCounter(counter));

    const key = await crypto.subtle.importKey('raw', keyData, {name: 'HMAC', hash: 'SHA-1'}, false, ['sign']);
    const signature = await crypto.subtle.sign({name: 'HMAC', hash: 'SHA-1'}, key, formatedCounter);

    // Now that we have the hash, we need to perform the HOTP specific byte selection
    // (called dynamic truncation in the RFC)
    const signatureArray = new Uint8Array(signature);
    const offset = signatureArray[signatureArray.length - 1] & 0xf;
    const binary =
        ((signatureArray[offset] & 0x7f) << 24) |
        ((signatureArray[offset + 1] & 0xff) << 16) |
        ((signatureArray[offset + 2] & 0xff) << 8) |
        (signatureArray[offset + 3] & 0xff);

    const otp = ('000000' + binary).slice(-6);

    return otp;
};

export async function getTOTP(secret) {
    const epoch = getEpoch();
    const otp = await getHOTP(secret, epoch);
    return otp;
};
