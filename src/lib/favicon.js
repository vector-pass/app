export const getFaviconUrl = (domain) =>
    `https://favicon.yandex.net/favicon/${domain}?size=32`;

const emptyFaviconBase64 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR4AWIAAYAAAwAABQABggWTzwAAAABJRU5ErkJggg';

export const fallbackFavicon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH5QQTDykX8bgjMQAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAJDSURBVFjD7VfNsdpADJbs4OHqV4Jz9mUXdwAlQAlQQighlPAoAZeAO7C1F59xCfGV2RlWuaxn/BwvXr+XTC7ohvdHn76V9AmAl/1nw7kHsixLjDFbREyYuQWAxC41zJwrpeifABBC7BHx3bFMFkgMAC0zH5VS578CIE3TOIqiGwAAMx8Rcc/MLSJK67CzHAC2vd9tEASrsiybTwPIsmxtjLkCACDiuaqqQ7cmpawAQHoEuSOi3LUYuBaklNvOuY0+Hmw5dZET0bNALkKI/SwAaZrGAHAZfP5AJRHlRIRa68PkOyO+Z1mWeAOIoug6/Ka1Po3treu6lVL+nAJhjLl45YAQQiJiNfjcEtHbkyT95ZPxQRBsyrIsphgYiyZ2URhF0Q/fUn48Hn/s/TbyXmsHhbeOMdsT9jYvtt5NZ+RunEnnSWt9GtnTDnqC07TWb3Vdt6NPsFgskonzLrpjXxaWy6V05kAYhpMX2egbANgNS9MzDxInAGOMbySJ7RMf9jNzMXUwDMPGCUBrXcwMKO45XymlNrYrOtm53+/k1YpnWtOXYSLKtdYrm5zz1FBKeetpPHy2wQzuZFdDG2Pg/NXEGopa75mOXnLcQzyHhe9j2t+/a0w1XTmwmwvAGHMTQsiB86oX/cFbDYko9ympMfIcA0vuGtGcVaCU2nwCBFmtuHbOmbkgot3siagD0Zt8Jkuxe/Oe6JzsHV+biu2McHlWnsxc9By3zLzxGdFn/S+wQ+oeANYjAtQgYoGI+bOe8LKXDe03mVAMpFG+NVwAAAAASUVORK5CYII';

export const getBase64FromUrl = async (url) => {
    const data = await fetch(url);
    const blob = await data.blob();

    return new Promise((resolve) => {
        const reader = new FileReader();

        reader.readAsDataURL(blob);

        reader.onloadend = function () {
            const base64data = reader.result.replace(/\=+$/, '');

            resolve(base64data);
        };
    });
};

export const getBase64FromDomain = async (domain) => {
    const faviconUrl = getFaviconUrl(domain);
    const iconBase64 = await getBase64FromUrl(faviconUrl);
    if (iconBase64 === emptyFaviconBase64) return fallbackFavicon;
    return iconBase64;
};

