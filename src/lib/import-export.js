export const exportToJsonFile = (jsonData, filename) => {
    const dataStr = JSON.stringify(jsonData, null, 4);
    const dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(dataStr);

    const linkElement = document.createElement('a');

    linkElement.setAttribute('href', dataUri);
    linkElement.setAttribute('download', filename);

    linkElement.click();
};

export const importFromFile = (file) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsText(file, "UTF-8");
    reader.onload = (evt) => {
        const data = JSON.parse(evt.target.result);
        resolve(data);
    };
    reader.onerror = () => {
        reject('Error reading file');
    };
});

export const importFromUrl = async (url) => {
    const response = await fetch(url);
    const json = await response.json();
    return json;
};
