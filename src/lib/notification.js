const buildNotificaion = (message) => {
    navigator.serviceWorker.ready.then((registration) => {
        registration.showNotification(message, {
            icon: '../img/icon-196x196.png',
            // To avoid skipping notifications on the same channel it
            // gets generated dynamically
            tag: `notify:${new Date().toISOString()}`
        });
    });
};

export const sendNotification = (message) => {
    if (!("Notification" in window)) {
        // Check if the browser supports notifications
        console.warn("This browser does not support desktop notification");
    } else if (Notification.permission === "granted") {
        // Check whether notification permissions have already been granted;
        // if so, create a notification
        buildNotificaion(message);
    } else if (Notification.permission !== "denied") {
        // We need to ask the user for permission
        Notification.requestPermission().then((permission) => {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                buildNotificaion(message);
            }
        });
    }

    // At last, if the user has denied notifications, and you
    // want to be respectful there is no need to bother them anymore.
};
