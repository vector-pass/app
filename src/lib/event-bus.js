export class EventBus {
    constructor(description) {
        this._bus = document.createComment(description);
    }

    addEventListener(type, listener) {
        this._bus.addEventListener(type, listener);
    }

    removeEventListener(type, listener) {
        this._bus.removeEventListener(type, listener);
    }

    fireEvent(event, detail={}) {
        this._bus.dispatchEvent(
            new CustomEvent(event, { detail })
        );
    }
}
