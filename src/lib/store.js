export const createStore = (initialState, updateCallback) => {
    const store = {
        ...initialState,
    };

    let nextUpdateCallback = null;

    return new Proxy(store, {
        set(target, key, value) {
            const reflectValue = Reflect.set(target, key, value);

            // Batch callback whe using setter multiple times
            if (nextUpdateCallback) cancelAnimationFrame(nextUpdateCallback);
            nextUpdateCallback = requestAnimationFrame(() => updateCallback(store));

            return reflectValue;
        }
    });
};
