import { html } from '../vendor/uhtml.js';
import WebComponent from '../lib/web-component.js';
import { createStore } from '../lib/store.js';

class ConfirmDeletePanel extends WebComponent {
    #store;

    static useShadow = true;

    constructor() {
        super();

        const initialState = {
            service: '',
            id: null,
        };

        this.#store = createStore(initialState, () => {
            this._render();
        });
    }

    get template() {
        return html`
            <vc-dialog ref="dialog" name="confirmDelete" heading="${`Delete ${this.#store.service}?`}">
                <iconify-icon slot="icon" icon="eva:trash-outline" height="32"></iconify-icon>
                <p>${`Do you really want to delete "${this.#store.service}"?`}</p>
                <p>This action is irreversible without a backup.</p>
                <div class="Button-group">
                    <button class="Button" @click="${() => this.cancel()}">Cancel</button>
                    <button class="Button Button--error" @click="${() => this.confirmDelete()}">Delete</button>
                </div>
            </vc-dialog>
        `;
    }

    askConfirm(service, id) {
        this.#store.service = service;
        this.#store.id = id;
        this.$.dialog.showModal();
    }

    cancel() {
        this.$.dialog.close();
    }

    confirmDelete() {
        this.fireEvent('delete', {
            id: this.#store.id,
        });
        this.$.dialog.close();
    }
}

customElements.define('vc-confirm-delete-panel', ConfirmDeletePanel);
