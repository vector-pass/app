import { html } from '../vendor/uhtml.js';
import WebComponent from '../lib/web-component.js';

class ImportPanel extends WebComponent {
    static useShadow = true;

    get template() {
        return html`
            <vc-dialog ref="dialog" name="import" heading="Import data">
                <iconify-icon slot="icon" icon="eva:cloud-upload-outline" height="32"></iconify-icon>
                <form class="Form" @submit="${(e) => this.submit(e)}">
                    <input class="Input" type="url" name="url" placeholder="URL" />
                    <input class="Input" type="file" name="file" />
                    <button class="Button">Import</button>
                </form>
            </vc-dialog>
        `;
    }

    submit(e) {
        e.preventDefault();

        const formEl = e.target;

        const formData = new FormData(formEl);

        const url = formData.get('url').trim();
        const file = formData.get('file');

        this.fireEvent('import', { url, file });

        formEl.reset();

        this.$.dialog.close();
    }
}

customElements.define('vc-import-panel', ImportPanel);
