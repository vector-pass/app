import { html } from '../vendor/uhtml.js';
import WebComponent from '../lib/web-component.js';

class TransferAccountPanel extends WebComponent {
    static useShadow = true;

    get template() {
        return html`
            <vc-dialog ref="dialog" name="transferAccount" heading="Transfer Account">
                <iconify-icon slot="icon" icon="eva:swap-outline" height="32"></iconify-icon>

                <p>Transferring account will copy all the services ensuring all the encrypted data are still readable in the other account.</p>
                <p>Any existing services will be kept unchanged after trasferring, even if is duplicated.</p>

                <form class="Form" @submit="${(e) => this.submit(e)}">
                    <input class="Input" type="text" name="username" placeholder="Target Username" autocomplete="username" autocapitalize="off" required />
                    <input class="Input" type="password" name="password" placeholder="Target Password" autocomplete="current-password" autocapitalize="off" required />
                    <button class="Button">Transfer services</button>
                </form>
            </vc-dialog>
        `;
    }

    submit(e) {
        e.preventDefault();

        const formEl = e.target;
        const formData = new FormData(formEl);

        const targetUsername = formData.get('username').trim();
        const targetPassword = formData.get('password').trim();

        this.fireEvent('transfer', { targetUsername, targetPassword });

        formEl.reset();

        this.$.dialog.close();
    }
}

customElements.define('vc-transfer-account-panel', TransferAccountPanel);
