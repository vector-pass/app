import { html } from '../vendor/uhtml.js';
import WebComponent from '../lib/web-component.js';

class OTPCodesPanel extends WebComponent {
    static useShadow = true;

    get template() {
        return html`
            <vc-dialog name="otpCodes" heading="OTP Codes">
                <iconify-icon slot="icon" icon="eva:shield-fill" height="32"></iconify-icon>
                <p>Select a service to copy the OTP Code.</p>
                <div class="List-container List-container--tight">
                    <slot></slot>
                </div>
            </vc-dialog>
        `;
    }
}

customElements.define('vc-otp-codes-panel', OTPCodesPanel);
