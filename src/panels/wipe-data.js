import { html } from '../vendor/uhtml.js';
import WebComponent from '../lib/web-component.js';

class WipeDataPanel extends WebComponent {
    static useShadow = true;

    get template() {
        return html`
            <vc-dialog ref="dialog" name="wipeData" heading="Wipe data">
                <iconify-icon slot="icon" icon="eva:alert-triangle-outline" height="32"></iconify-icon>
                <p>By wiping data all your accounts and other settings will be deleted permanently.</p>
                <p>It is recommended to do a backup before proceding.</p>
                <div class="Button-group">
                    <button class="Button" @click="${() => this.exportData()}">Export data</button>
                    <button class="Button Button--error" @click="${() => this.wipeData()}">Wipe data</button>
                </div>
            </vc-dialog>
        `;
    }

    exportData() {
        this.fireEvent('export');
    }

    wipeData() {
        this.fireEvent('wipedata');
        this.$.dialog.close();
    }
}

customElements.define('vc-wipe-data-panel', WipeDataPanel);
