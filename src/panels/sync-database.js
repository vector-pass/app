import { html } from '../vendor/uhtml.js';
import WebComponent from '../lib/web-component.js';

class SyncDatabasePanel extends WebComponent {
    static useShadow = true;

    get template() {
        return html`
            <vc-dialog ref="dialog" name="syncDatabase" heading="Sync database">
                <iconify-icon slot="icon" icon="eva:sync-outline" height="32"></iconify-icon>

                <p>Assign a remote CouchDB database URL to enable syncing.</p>
                <p>The format is <code>http[s]://DOMAIN:PORT/DATABASE_NAME</code></p>
                <p>Mind that wiping the database will not alter the remote database.</p>

                <form class="Form" @submit="${(e) => this.submit(e)}">
                    <input class="Input" type="url" name="remote-database" placeholder="Remote Database" autocapitalize="off" autocomplete="off" required />
                    <button class="Button">Sync Database</button>
                </form>
            </vc-dialog>
        `;
    }

    submit(e) {
        e.preventDefault();

        const formEl = e.target;
        const formData = new FormData(formEl);

        const remoteDatabase = formData.get('remote-database').trim();

        this.fireEvent('sync', { remoteDatabase });

        formEl.reset();

        this.$.dialog.close();
    }
}

customElements.define('vc-sync-database-panel', SyncDatabasePanel);
