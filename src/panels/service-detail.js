import { html } from '../vendor/uhtml.js';
import WebComponent from '../lib/web-component.js';
import { createStore } from '../lib/store.js';
import { getAccount } from '../core/account.js';

class ServiceDetailPanel extends WebComponent {
    #store;

    static useShadow = true;

    constructor() {
        super();

        const initialState = {
            currentAccount: getAccount({}),
            fields: [],
        };

        this.#store = createStore(initialState, () => {
            this._render();
        });
    }

    get template() {
        return html`
            <vc-dialog ref="dialog" heading="${this.#store.currentAccount.name || this.#store.currentAccount.service}">
                <vc-service-icon
                    slot="icon"
                    favicon="${this.#store.currentAccount.favicon}"
                    ?favorite="${this.#store.currentAccount.favorite}"
                ></vc-service-icon>

                <div class="Field-container">
                    ${this.getFields(this.#store.fields)}
                </div>

                <div class="Button-group">
                    <button class="Button" @click="${this.openForm}">Edit</button>
                    <button class="Button Button--error" @click="${this.delete}">Delete</button>
                </div>
            </vc-dialog>
        `;
    }

    getFields(fields) {
        return fields.map(({ label, value, copy, url, isOTP, isPrivate }) => html`
            <vc-field-display
                label="${label}"
                value="${value}"
                copy="${copy}"
                url="${url}"
                ?private="${isPrivate}"
                ?otp="${isOTP}"
                .hideValue="${isPrivate}"
            ></vc-field-display>
        `);
    }

    showDetail(account, fields) {
        this.#store.currentAccount = account;
        this.#store.fields = fields;
        this.$.dialog.showModal();
    }

    openForm = () => {
        this.fireEvent('openform', {
            id: this.#store.currentAccount._id,
        });
        this.$.dialog.close();
    };

    delete = () => {
        this.fireEvent('confirmdelete', {
            id: this.#store.currentAccount._id,
        });
        this.$.dialog.close();
    };
}

customElements.define('vc-service-detail-panel', ServiceDetailPanel);
