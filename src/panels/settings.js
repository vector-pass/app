import { html } from '../vendor/uhtml.js';
import WebComponent from '../lib/web-component.js';

class SettingsPanel extends WebComponent {
    static useShadow = true;

    get template() {
        return html`
            <vc-dialog name="settings" heading="Settings">
                <iconify-icon slot="icon" icon="eva:options-fill" height="32"></iconify-icon>
                <div class="Item-container">
                    <vc-item
                        icon="eva:cloud-download-outline"
                        label="Export data"
                        @click="${() => this.exportData()}"
                    ></vc-item>

                    <vc-dialog-trigger show="import">
                        <vc-item
                            icon="eva:cloud-upload-outline"
                            label="Import data"
                        ></vc-item>
                    </vc-dialog-trigger>

                    <vc-install-button>
                        <vc-item
                            icon="eva:arrow-circle-down-outline"
                            label="Install"
                        ></vc-item>
                    </vc-install-button>

                    <vc-dialog-trigger show="transferAccount" hidden>
                        <vc-item
                            icon="eva:swap-outline"
                            label="Transfer account"
                        ></vc-item>
                    </vc-dialog-trigger>

                    <vc-dialog-trigger show="syncDatabase">
                        <vc-item
                            icon="eva:sync-outline"
                            label="Sync database"
                        ></vc-item>
                    </vc-dialog-trigger>

                    <vc-dialog-trigger show="wipeData">
                        <vc-item
                            icon="eva:alert-triangle-outline"
                            label="Wipe data"
                        ></vc-item>
                    </vc-dialog-trigger>

                    <vc-item
                        icon="eva:log-out-outline"
                        label="Logout"
                        @click="${() => this.logout()}"
                    ></vc-item>
                </div>
            </vc-dialog>
        `;
    }

    exportData() {
        this.fireEvent('export');
    }

    async logout() {
        this.fireEvent('logout', {}, { bubble: true, composed: true });
    }
}

customElements.define('vc-settings-panel', SettingsPanel);
