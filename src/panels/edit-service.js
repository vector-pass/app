import { html } from '../vendor/uhtml.js';
import WebComponent from '../lib/web-component.js';
import { createStore } from '../lib/store.js';
import { getAccount } from '../core/account.js';
import { pathsToObject } from '../lib/paths-to-object.js';

class EditServicePanel extends WebComponent {
    #store;

    static useShadow = true;

    constructor() {
        super();

        const initialState = {
            currentAccount: getAccount({}),
            fieldset: [],
            multiRows: {},
        };

        this.#store = createStore(initialState, () => {
            this._render();
        });
    }

    get template() {
        const heading = this.#store.currentAccount._id
            ? `Edit ${this.#store.currentAccount.service}`
            : `Add New Service`;
        return html`
            <vc-dialog ref="dialog" heading="${heading}">
                <vc-service-icon
                    slot="icon"
                    favicon="${this.#store.currentAccount.favicon}"
                    ?favorite="${this.#store.currentAccount.favorite}"
                ></vc-service-icon>

                <form ref="form" class="Form" method="dialog" @submit="${this.#submit}">
                    <input type="hidden" name="id" value="${this.#store.currentAccount._id}" />

                    ${this.getFieldSet(this.#store.fieldset)}

                    <button class="Button">Edit Service</button>
                </form>

            </vc-dialog>
        `;
    }

    getFieldSet(fieldset=[]) {
        return fieldset.map(({ label, fields }) => html`
            <fieldset class="Form-fieldset">
                <legend class="Form-legend">${label}</legend>

                ${this.getFields(fields)}
            </fieldset>
        `);
    }

    getFields(fields) {
        return fields.map(({ type, ...data }) => {
            switch (type) {
                case 'label-text':
                    return this.getInputLabel(this.getTextField, data);
                case 'number':
                    return this.getInputLabel(this.getNumberField, data);
                case 'checkbox':
                    return this.getCheckboxField(data);
                case 'row':
                    return this.getFieldRow(data);
                case 'multi-row':
                    return this.getMultiRow(data);
                default:
                    return this.getTextField(data);
            }
        });
    }

    getTextField({ label, name, value, required }) {
        return html`
            <input
                class="Input"
                type="text"
                name="${name}"
                value="${value}"
                placeholder="${label}"
                autocapitalize="off"
                autocomplete="off"
                ?required="${required}"
            />
        `;
    }

    getNumberField({ label, name, value, required }) {
        return html`
            <input
                class="Input"
                type="number"
                name="${name}"
                value="${value}"
                placeholder="${label}"
                step="1"
                ?required="${required}"
            />
        `;
    }

    getCheckboxField({ label, name, value, required }) {
        return html`
            <label>
                <span>${label}</span>
                <input
                    class="Checkbox"
                    type="checkbox"
                    name="${name}"
                    ?checked="${value}"
                    ?required="${required}"
                />
            </label>
        `;
    }

    getFieldRow({ fields }) {
        return html`
            <div class="Form-row">
                ${this.getFields(fields)}
            </div>
        `;
    }

    // TODO: Make this work
    getMultiRow({ name, values, fields }) {
        const valuesLength = (this.#store.multiRows[name] || 0);

        const valueRows = Array(valuesLength).fill(null).map((_, index) => {
            const value = values[index] || {};

            const fieldsData = fields.map(field => ({
                ...field,
                name: `${name}.${index}.${field.name}`,
                value: value[field.name] || '',
            }));

            const disabledName = `${name}.${index}.disabled`;

            return html`
                <div class="Form-row">
                    ${this.getFields(fieldsData)}

                    <input type="hidden" name="${disabledName}" value="false" />

                    <button
                        class="IconButton Button--outline"
                        type="button"
                        @click="${(e) => this.#deleteField(e, disabledName)}"
                    >
                        <iconify-icon icon="eva:trash-outline" height="24"></iconify-icon>
                    </button>
                </div>
            `;
        });

        return html`
            ${valueRows}

            <div class="Button-group Button-group--noMargin">
                <button
                    class="Button Button--flat Button--outline"
                    type="button"
                    @click="${(e) => this.#addField(e, name)}"
                >Add field</button>
            </div>
        `;
    }

    #addField(e, name) {
        e.preventDefault();

        this.#store.multiRows = {
            ...this.#store.multiRows,
            [name]: this.#store.multiRows[name] + 1,
        };
    }

    #deleteField(e, name) {
        e.preventDefault();

        const formRowEl = e.target.closest('.Form-row');
        formRowEl.setAttribute('hidden', 'hidden');

        const disabledFieldEl = formRowEl.querySelector(`[name="${name}"]`);
        disabledFieldEl.value = true;

        const requiredEls = formRowEl.querySelectorAll('[required]');
        requiredEls.forEach(el => el.removeAttribute('required'));
    }

    getInputLabel(input, { label, ...data }) {
        return html`
            <label class="InputLabel">
                <div class="InputLabel-label Field-label">${label}</div>
                ${input({ label, ...data })}
            </label>
        `;
    }

    showForm(account, fieldset) {
        this.#store.currentAccount = account;
        this.#store.fieldset = fieldset;

        fieldset.forEach(({ fields }) => {
            this.#store.multiRows = {
                ...this.#store.multiRows,
                ...fields
                    .filter(field => field.type === 'multi-row')
                    .reduce((rows, { name, values }) => ({ ...rows, [name]: values.length }), {}),
            };
        });

        this.$.dialog.showModal();
    }

    #submit = (e) => {
        e.preventDefault();

        const formData = new FormData(e.target);

        const data = pathsToObject(Array.from(formData));
        this.fireEvent('formsaved', data);

        this.$.form.reset();
        this.$.dialog.close();
    };
}

customElements.define('vc-edit-service-panel', EditServicePanel);
