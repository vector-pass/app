import './polyfill.js';

import './vendor/custom-elements.js';
import './vendor/iconify-icon.js';

import './ui/components/card.js';
import './ui/components/item.js';
import './ui/components/dialog.js';
import './ui/components/logo.js';
import './ui/components/service-icon.js';
import './ui/components/otp-code.js';
import './ui/components/install-button.js';

import './ui/compound/fab.js';
import './ui/compound/quick-action.js';
import './ui/compound/service-favorite.js';
import './ui/compound/service-card.js';
import './ui/compound/otp-service-card.js';
import './ui/compound/header.js';
import './ui/components/icon-button.js';
import './ui/components/field-display.js';

import './views/app.js';
import './views/login.js';
import './views/list.js';

import './panels/service-detail.js';
import './panels/edit-service.js';
import './panels/confirm-delete.js';
import './panels/otp-codes.js';
import './panels/settings.js';
import './panels/import.js';
import './panels/transfer-account.js';
import './panels/wipe-data.js';
import './panels/sync-database.js';
