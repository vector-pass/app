import { html } from '../vendor/uhtml.js';
import WebComponent from '../lib/web-component.js';
import { Core } from '../core/index.js';
import { getAccount } from '../core/account.js';

class App extends WebComponent {
    #core;

    static useShadow = false;

    static listenedEvents = [
        'login',
        'logout',
        'export',
        'import',
        'sync',
        'transfer',
        'wipedata',
        'addnew',
        'showdetail',
        'openform',
        'formsaved',
        'confirmdelete',
        'delete',
    ];

    constructor() {
        super();

        this.#core = new Core(() => {
            this._render();
        });
    }

    async firstRenderCallback() {
        await this.#core.init();
    }

    get template() {
        if (!this.#core.auth.isAuthenticated) return this.templateLoginForm();
        return this.templateMain();
    }

    templateLoginForm() {
        return html`
            <vc-login-view></vc-login-view>
        `;
    }

    templateMain() {
        return html`
            <vc-list-view
                username="${this.#core.auth.username}"
                .auth="${this.#core.auth}"
                ?hasotp="${this.#core.otp.length}"
                ?hasfavorites="${this.#core.favorites.length}"
            >
                ${this.#core.favorites.map(
                    account => html.for(account, account._id)`
                        <vc-service-favorite
                            slot="favorites"
                            service="${account.name || account.service}"
                            favicon="${account.favicon}"
                            accountid="${account._id}"
                        ></vc-service-favorite>
                    `
                )}

                <span slot="accountscount">${`(${this.#core.accounts.length})`}</span>

                ${this.#core.accounts.map(
                    account => html.for(account, account._id)`
                        <vc-service-card
                            service="${account.name || account.service}"
                            username="${account.username}"
                            favicon="${account.favicon}"
                            ?favorite="${account.favorite}"
                            accountid="${account._id}"
                        ></vc-service-card>
                    `
                )}
            </vc-list-view>

            <vc-otp-codes-panel>
                ${this.#core.otp.map(
                    account => html.for(account, account._id)`
                        <vc-otp-service-card
                            service="${account.name || account.service}"
                            username="${account.username}"
                            favicon="${account.favicon}"
                            ?favorite="${account.favorite}"
                            accountid="${account._id}"
                            secret="${account.otp}"
                        ></vc-otp-service-card>
                    `
                )}
            </vc-otp-codes-panel>

            <vc-service-detail-panel ref="detail"></vc-service-detail-panel>

            <vc-edit-service-panel ref="form"></vc-edit-service-panel>

            <vc-confirm-delete-panel ref="confirmDelete"></vc-confirm-delete-panel>

            <vc-settings-panel></vc-settings-panel>

            <vc-import-panel></vc-import-panel>

            <vc-sync-database-panel ref="sync"></vc-sync-database-panel>

            <vc-transfer-account-panel></vc-transfer-account-panel>

            <vc-wipe-data-panel></vc-wipe-data-panel>

            <vc-sync-database></vc-sync-database>
        `;
    }

    async onlogin(e) {
        const { username, password } = e.detail;
        await this.#core.login(username, password);
    }

    async onlogout() {
        await this.#core.logout();
    }

    async onexport() {
        await this.#core.exportData();
    }

    async onimport(e) {
        const { file, url } = e.detail;
        await this.#core.importData(file, url);
    }

    async onsync(e) {
        const { remoteDatabase } = e.detail;
        await this.#core.syncDatabase(remoteDatabase);
    }

    async ontransfer(e) {
        const { targetUsername, targetPassword } = e.detail;
        console.log('TRANSFER');
    }

    async onwipedata() {
        await this.#core.wipeData();
    }

    async onshowdetail(e) {
        const { id } = e.detail;

        const account = await this.#core.getAccount(id);
        const fields = await this.#core.getAccountDisplayFields(account);

        this.$.detail.showDetail(account, fields);
    }

    async onaddnew() {
        const account = getAccount({});
        const fields = await this.#core.getAccountFormFields(account);

        this.$.form.showForm(account, fields);
    }

    async onopenform(e) {
        const { id } = e.detail;

        const account = await this.#core.getAccount(id);
        const fields = await this.#core.getAccountFormFields(account);

        this.$.form.showForm(account, fields);
    }

    async onconfirmdelete(e) {
        const { id } = e.detail;

        const account = await this.#core.getAccount(id);
        this.$.confirmDelete.askConfirm(account.service, account._id);
    }

    async ondelete(e) {
        const { id } = e.detail;

        await this.#core.deleteAccount(id);
    }

    async onformsaved(e) {
        const { id, ...data } = e.detail;

        if (id) {
            await this.#core.saveAccount(id, data);
        } else {
            await this.#core.createAccount(data);
        }
    }
}

customElements.define('vc-app', App);
