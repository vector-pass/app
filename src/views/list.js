import { html } from '../vendor/uhtml.js';
import WebComponent from '../lib/web-component.js';

class ListView extends WebComponent {
    static useShadow = true;

    static observedAttributes = ['username', 'hasotp', 'hasfavorites'];

    get template() {
        return html`
            <div class="Main">
                <vc-header
                    class="Main-header"
                    username="${this.username}"
                ></vc-header>

                <div class="Main-quickActions QuickActions">
                    <vc-quick-action
                        title="OTP Codes"
                        icon="eva:shield-fill"
                        show="otpCodes"
                        ?hidden="${!this.hasAttribute('hasotp')}"
                    ></vc-quick-action>
                </div>

                <div class="Main-favorites Favorites" ?hidden="${!this.hasAttribute('hasfavorites')}">

                    <h2 class="Favorites-heading">Favorites</h2>

                    <div class="Favorites-container">
                        <slot name="favorites"></slot>
                    </div>

                </div>

                <div class="Main-list List">

                    <h2 class="List-heading">
                        Accounts
                        <slot name="accountscount"></slot>
                    </h2>

                    <div class="List-container">
                        <slot></slot>
                    </div>

                </div>

                <vc-fab class="Main-fab"></vc-fab>
            </div>
        `;
    }
}

customElements.define('vc-list-view', ListView);
