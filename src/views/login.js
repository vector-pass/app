import { html } from '../vendor/uhtml.js';
import WebComponent from '../lib/web-component.js';
import { InputLabel } from '../ui/components.js';

class LoginView extends WebComponent {
    static useShadow = false;

    get template() {
        return html`
            <div class="Login">
                <header class="Login-header Header Header--center">
                    <vc-logo big></vc-logo>
                </header>

                <div class="Login-form Card">
                    <h2 class="Center">Login</h2>
                    <p class="Center">Enter your credentials to login.</p>
                    <form class="Form" @submit="${(e) => this.login(e)}">
                        ${InputLabel('Username', html`
                            <input
                                class="Input"
                                type="text"
                                name="username"
                                placeholder="Username"
                                autocomplete="username"
                                autocapitalize="off"
                                required
                                autofocus
                            />
                        `)}
                        ${InputLabel('Password', html`
                            <input
                                class="Input"
                                type="password"
                                name="password"
                                placeholder="Password"
                                autocomplete="current-password"
                                autocapitalize="off"
                                required
                            />
                        `)}
                        <button class="Button" type="submit">Login</button>
                    </form>
                </div>
            </div>
        `;
    }

    login(e) {
        e.preventDefault();

        const formData = new FormData(e.target);

        const username = formData.get('username').trim();
        const password = formData.get('password').trim();

        this.fireEvent('login', {
            username,
            password,
        });
    }
}

customElements.define('vc-login-view', LoginView);
