module.exports = {
	globDirectory: 'public/',
	globPatterns: [
		'**/*.{png,svg,html,js,webmanifest,css}'
	],
	swDest: 'public/sw.js',
	ignoreURLParametersMatching: [
		/^utm_/,
		/^fbclid$/
	]
};
