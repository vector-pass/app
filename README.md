# Vector Pass

Vector Pass is a deterministic password manager with some extra features.

To use Vector Pass just go to <https://vector-pass.gitlab.io/app/> and use the app.


## How it works

Vector Pass rely on [deterministic generation](https://en.wikipedia.org/wiki/Deterministic_algorithm) to create passwords and other data on the fly.

The app generates a token based on a username / password pair and use it to output passwords and other fields based on some simple rules.


## Warning for users

Due to how browsers manage the sites storage sometimes they can wipe your data from the app, especially when your device storage is low, keeping a regular backup of your data is highly recommend, they can be done easily with the export functionality.


## Features

Here's the main features Vector Pass provides you:

- **Local first**  
  Everything stored never leaves your device, unless you decide to.

- **Progressive Web App**  
  The web app can be installed on any modern device and be used like a native app even without network connectivity.

- **Deterministic generation**  
  The actual passwords are generated on the fly once you log in and are not stored anywhere.

- **Not only for passwords**  
  PINs and temporary emails (thanks to [Maildrop.cc](https://maildrop.cc/)) can also be generated.

- **Custom Field**  
  Add any kind of custom information by relying on a [XOR operation](https://en.wikipedia.org/wiki/Exclusive_or) to store them safely.

- **OTP**  
  Store your OTP key string and the app will generate a Google Authenticator compatible code for you, there's also a dedicated panel to launch them quickly.

- **Multi-user**  
  The app user an account-like login to allow the use of multiple users on the same devices, with the ability to transfer data between them.

- **Import and Export**  
  You can freely import / export all your data with a simple json file.

- **Wipe data**  
  The Secret Organization found your hideout? Quickly wipe everything in one go!

- **Offline-first data sync**  
  You can sync your data with a [CouchDB](https://couchdb.apache.org/) instance of your choice and have an offline-first realtime backup.

- **No tracking**  
  There's no tracking, analytics or crash reporting so nobody will see your own data, ever.


## Development

The project is made to be free from any build step in development and can be used with any static web server so `nodejs` is an optional requirement.

```bash
# For working with live-reload:
npm start

# If you don't have/want nodejs you can...
python3 -m http.server 5000
```


## Contributing

Pull requests are welcome from everyone. For major changes, please open an issue first to discuss what you would like to change.


## Motivation

As I was looking for a way to safely store my own passwords I looked at many tools and services. But, in the end, I never really used anything because I couldn't trust them entirely.

"Is it really safe?"

"Wont it get hacked someday?"

"Can I really access it anytime and anywhere?"

These questions bothered me for a long time but eventually I found a valid alternative: **Deterministic Password Managers!**

There was only one issue. Nothing I found could be used with the simplicity of more known password managers. They where too limited, complex or unfriendly. So I decided to work on my own take on the matter. And with that, **Vector Pass** was born with the capabilities of a deterministic password manager yet with all the handy features of common ones.


## A kind note

Like everything, Vector Pass is not a silver bullet and I **warmly** recommend anyone who is considering to use it to first learn more about the [risks](https://tonyarcieri.com/4-fatal-flaws-in-deterministic-password-managers) and [advantages](https://medium.com/@mahdix/in-defense-of-deterministic-password-managers-67b5a549681e) of using such tools  before jumping straight to it.


## License

The code is licensed with the [MIT](https://choosealicense.com/licenses/mit/) license.
